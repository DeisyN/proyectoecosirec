/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsi.proyectoSirec.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author krix
 */
@Entity
@Table(name = "calificaciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Calificaciones.findAll", query = "SELECT c FROM Calificaciones c"),
    @NamedQuery(name = "Calificaciones.findByIdCalificacion", query = "SELECT c FROM Calificaciones c WHERE c.idCalificacion = :idCalificacion"),
    @NamedQuery(name = "Calificaciones.findByCalificacion", query = "SELECT c FROM Calificaciones c WHERE c.calificacion = :calificacion"),
    @NamedQuery(name = "Calificaciones.findByIdEmpresaCalificaciones", query = "SELECT c FROM Calificaciones c WHERE c.idEmpresa.idUsuario = :idEmpresa"),
    @NamedQuery(name = "Calificaciones.findByComentario", query = "SELECT c FROM Calificaciones c WHERE c.comentario = :comentario")})
public class Calificaciones implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_calificacion")
    private Integer idCalificacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "calificacion")
    private short calificacion;
    @Size(min = 1, max = 100)
    @Column(name = "comentario")
    private String comentario;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuarios idEmpresa;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuarios idUsuario;

    public Calificaciones() {
    }

    public Calificaciones(Integer idCalificacion) {
        this.idCalificacion = idCalificacion;
    }

    public Calificaciones(Integer idCalificacion, short calificacion, String comentario) {
        this.idCalificacion = idCalificacion;
        this.calificacion = calificacion;
        this.comentario = comentario;
    }

    public Integer getIdCalificacion() {
        return idCalificacion;
    }

    public void setIdCalificacion(Integer idCalificacion) {
        this.idCalificacion = idCalificacion;
    }

    public short getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(short calificacion) {
        this.calificacion = calificacion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Usuarios getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Usuarios idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCalificacion != null ? idCalificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Calificaciones)) {
            return false;
        }
        Calificaciones other = (Calificaciones) object;
        if ((this.idCalificacion == null && other.idCalificacion != null) || (this.idCalificacion != null && !this.idCalificacion.equals(other.idCalificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsi.proyectoSirec.jpa.entities.Calificaciones[ idCalificacion=" + idCalificacion + " ]";
    }
    
}
