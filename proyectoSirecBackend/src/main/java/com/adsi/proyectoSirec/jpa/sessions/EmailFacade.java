/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsi.proyectoSirec.jpa.sessions;

import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.NamingException;

@Stateless
public class EmailFacade   {
    
 @Resource(name = "mail/misena")
    private Session mailSession;

    public void sendMail(String email, String emailUser, String subject, String body) throws NamingException, MessagingException {
        Message message = new MimeMessage(mailSession);
        message.setFrom(new InternetAddress(emailUser));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
        message.setSubject(subject);
        message.setText(body);
        //message.setSentDate(new Date());
        Transport.send(message);
    }

    public void sendMailHtml(String email, String subject, String body) throws NamingException, MessagingException {
        Message message = new MimeMessage(mailSession);
        message.setFrom(new InternetAddress(email));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
        message.setSubject(subject);
        message.setContent(body, "text/html; charset=utf-8");
        //message.setSentDate(new Date());
        Transport.send(message);
    }
}
