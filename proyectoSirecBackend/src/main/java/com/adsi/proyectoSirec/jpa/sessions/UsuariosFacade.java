/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsi.proyectoSirec.jpa.sessions;

import com.adsi.proyectoSirec.jpa.entities.Usuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author krix
 */
@Stateless
public class UsuariosFacade extends AbstractFacade<Usuarios> {
    @PersistenceContext(unitName = "com.mycompany_proyectoSirecBackend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuariosFacade() {
        super(Usuarios.class);
    }
    
       public Usuarios findByEmail(String email) {
        try {
            return (Usuarios) getEntityManager().createNamedQuery("Usuarios.findByEmail")
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NonUniqueResultException ex) {
            throw ex;
        } catch (NoResultException ex) {
            return null;
        }
    }
    
    public Usuarios findByRazonSocial(String razonSocial) {
        try {
            return (Usuarios) getEntityManager().createNamedQuery("Usuarios.findByRazonSocial")
                    .setParameter("razonSocial", razonSocial)
                    .getSingleResult();
        } catch (NonUniqueResultException ex) {
            throw ex;
        } catch (NoResultException ex) {
            return null;
        }
    }
    
    
    public List<Usuarios> findByNombre(String nombre) {
        return getEntityManager().createNamedQuery("Usuarios.findByNombre")
                .setParameter("nombre", nombre + "%")
                .getResultList();
    }
    
    public List<Usuarios> findByTipoUsuario(Integer idTipoUsuario) {
        return getEntityManager().createNamedQuery("Usuarios.findByIdTipoUsuario")
                .setParameter("idTipoUsuario", idTipoUsuario)
                .getResultList();
    }
     
    public List<Usuarios> findByRol(String idRol) {
        return getEntityManager().createNamedQuery("Usuarios.findByIdRol")
                .setParameter("idRol", idRol)
                .getResultList();
    }
      
    public Usuarios findByIdAndCodigo(Integer idUsuario, String codigoRecuperacionPass){
        return (Usuarios) getEntityManager().createNamedQuery("Usuarios.findByIdAndCodigo")
                .setParameter("idUsuario", idUsuario)
                .setParameter("codigoRecuperacionPass", codigoRecuperacionPass)
                .getSingleResult();
    } 
    
}
