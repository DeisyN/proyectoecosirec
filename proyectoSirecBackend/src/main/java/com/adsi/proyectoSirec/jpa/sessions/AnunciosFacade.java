/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsi.proyectoSirec.jpa.sessions;

import com.adsi.proyectoSirec.jpa.entities.Anuncios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author krix
 */
@Stateless
public class AnunciosFacade extends AbstractFacade<Anuncios> {
    @PersistenceContext(unitName = "com.mycompany_proyectoSirecBackend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AnunciosFacade() {
        super(Anuncios.class);
    }
    
    public List<Anuncios> findByIdUsuario(int idUsuario) {
        return getEntityManager().createNamedQuery("Anuncios.findByIdUsuario")
                .setParameter("idUsuario", idUsuario)
                .getResultList();
    }
    
}
