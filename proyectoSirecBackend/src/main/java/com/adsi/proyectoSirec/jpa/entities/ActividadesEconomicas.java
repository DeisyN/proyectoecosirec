/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsi.proyectoSirec.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lorena
 */
@Entity
@Table(name = "actividades_economicas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ActividadesEconomicas.findAll", query = "SELECT a FROM ActividadesEconomicas a"),
    @NamedQuery(name = "ActividadesEconomicas.findByCodigo", query = "SELECT a FROM ActividadesEconomicas a WHERE a.codigo = :codigo"),
    @NamedQuery(name = "ActividadesEconomicas.findByDescripcion", query = "SELECT a FROM ActividadesEconomicas a WHERE a.descripcion = :descripcion")})
public class ActividadesEconomicas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codigo")
    private Integer codigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinTable(name = "actividades_economicas_has_mi_empresa", joinColumns = {
        @JoinColumn(name = "codigo", referencedColumnName = "codigo")}, inverseJoinColumns = {
        @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")})
    @ManyToMany
    private List<MiEmpresa> miEmpresaList;

    public ActividadesEconomicas() {
    }

    public ActividadesEconomicas(Integer codigo) {
        this.codigo = codigo;
    }

    public ActividadesEconomicas(Integer codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<MiEmpresa> getMiEmpresaList() {
        return miEmpresaList;
    }

    public void setMiEmpresaList(List<MiEmpresa> miEmpresaList) {
        this.miEmpresaList = miEmpresaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ActividadesEconomicas)) {
            return false;
        }
        ActividadesEconomicas other = (ActividadesEconomicas) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsi.proyectoSirec.jpa.entities.ActividadesEconomicas[ codigo=" + codigo + " ]";
    }
    
}
