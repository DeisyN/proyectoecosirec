/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsi.proyectoSirec.jpa.sessions;

import com.adsi.proyectoSirec.jpa.entities.MiEmpresa;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author krix
 */
@Stateless
public class MiEmpresaFacade extends AbstractFacade<MiEmpresa> {
    @PersistenceContext(unitName = "com.mycompany_proyectoSirecBackend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MiEmpresaFacade() {
        super(MiEmpresa.class);
    }
    
     public MiEmpresa findByNit(String nit) {
        try {
            return (MiEmpresa) getEntityManager().createNamedQuery("MiEmpresa.findByNit")
                    .setParameter("nit", nit + "%")
                    .getSingleResult();
        } catch (NonUniqueResultException ex) {
            throw ex;
        } catch (NoResultException ex) {
            return null;
        }
    }

    public List<MiEmpresa> findByNombre(String nombre) {
        return getEntityManager().createNamedQuery("MiEmpresa.findByNombre")
                .setParameter("nombre", nombre + "%")
                .getResultList();
    }
    
    public List<MiEmpresa> findByNitConsulta(String nit) {
        return getEntityManager().createNamedQuery("MiEmpresa.findByNit")
                .setParameter("nit", nit + "%")
                .getResultList();
    }
    
}
