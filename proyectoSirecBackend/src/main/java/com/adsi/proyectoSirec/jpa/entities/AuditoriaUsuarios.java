/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsi.proyectoSirec.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author krix
 */
@Entity
@Table(name = "auditoria_usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AuditoriaUsuarios.findAll", query = "SELECT a FROM AuditoriaUsuarios a"),
    @NamedQuery(name = "AuditoriaUsuarios.findByIdAuditoriaUsuario", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.idAuditoriaUsuario = :idAuditoriaUsuario"),
    @NamedQuery(name = "AuditoriaUsuarios.findByFecha", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.fecha = :fecha"),
    @NamedQuery(name = "AuditoriaUsuarios.findByNombreOld", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.nombreOld = :nombreOld"),
    @NamedQuery(name = "AuditoriaUsuarios.findByPrimerApellidoOld", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.primerApellidoOld = :primerApellidoOld"),
    @NamedQuery(name = "AuditoriaUsuarios.findBySegundoApellidoOld", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.segundoApellidoOld = :segundoApellidoOld"),
    @NamedQuery(name = "AuditoriaUsuarios.findByTelefonoOld", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.telefonoOld = :telefonoOld"),
    @NamedQuery(name = "AuditoriaUsuarios.findByDireccionOld", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.direccionOld = :direccionOld"),
    @NamedQuery(name = "AuditoriaUsuarios.findByEmailOld", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.emailOld = :emailOld"),
    @NamedQuery(name = "AuditoriaUsuarios.findByPasswordOld", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.passwordOld = :passwordOld"),
    @NamedQuery(name = "AuditoriaUsuarios.findByFechaNacimientoOld", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.fechaNacimientoOld = :fechaNacimientoOld"),
    @NamedQuery(name = "AuditoriaUsuarios.findByRazonSocialOld", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.razonSocialOld = :razonSocialOld"),
    @NamedQuery(name = "AuditoriaUsuarios.findByIdCiudadOld", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.idCiudadOld = :idCiudadOld"),
    @NamedQuery(name = "AuditoriaUsuarios.findByIdDepartamentoOld", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.idDepartamentoOld = :idDepartamentoOld"),
    @NamedQuery(name = "AuditoriaUsuarios.findByNombreNew", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.nombreNew = :nombreNew"),
    @NamedQuery(name = "AuditoriaUsuarios.findByPrimerApellidoNew", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.primerApellidoNew = :primerApellidoNew"),
    @NamedQuery(name = "AuditoriaUsuarios.findBySegundoApellidoNew", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.segundoApellidoNew = :segundoApellidoNew"),
    @NamedQuery(name = "AuditoriaUsuarios.findByTelefonoNew", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.telefonoNew = :telefonoNew"),
    @NamedQuery(name = "AuditoriaUsuarios.findByDireccionNew", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.direccionNew = :direccionNew"),
    @NamedQuery(name = "AuditoriaUsuarios.findByEmailNew", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.emailNew = :emailNew"),
    @NamedQuery(name = "AuditoriaUsuarios.findByPasswordNew", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.passwordNew = :passwordNew"),
    @NamedQuery(name = "AuditoriaUsuarios.findByFechaNacimientoNew", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.fechaNacimientoNew = :fechaNacimientoNew"),
    @NamedQuery(name = "AuditoriaUsuarios.findByRazonSocialNew", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.razonSocialNew = :razonSocialNew"),
    @NamedQuery(name = "AuditoriaUsuarios.findByIdCiudadNew", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.idCiudadNew = :idCiudadNew"),
    @NamedQuery(name = "AuditoriaUsuarios.findByIdDepartamentoNew", query = "SELECT a FROM AuditoriaUsuarios a WHERE a.idDepartamentoNew = :idDepartamentoNew")})
public class AuditoriaUsuarios implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_auditoria_usuario")
    private Integer idAuditoriaUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Size(max = 45)
    @Column(name = "nombre_old")
    private String nombreOld;
    @Size(max = 20)
    @Column(name = "primer_apellido_old")
    private String primerApellidoOld;
    @Size(max = 20)
    @Column(name = "segundo_apellido_old")
    private String segundoApellidoOld;
    @Size(max = 16)
    @Column(name = "telefono_old")
    private String telefonoOld;
    @Size(max = 45)
    @Column(name = "direccion_old")
    private String direccionOld;
    @Size(max = 50)
    @Column(name = "email_old")
    private String emailOld;
    @Size(max = 256)
    @Column(name = "password_old")
    private String passwordOld;
    @Column(name = "fecha_nacimiento_old")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimientoOld;
    @Size(max = 45)
    @Column(name = "razon_social_old")
    private String razonSocialOld;
    @Column(name = "id_ciudad_old")
    private Integer idCiudadOld;
    @Column(name = "id_departamento_old")
    private Integer idDepartamentoOld;
    @Size(max = 45)
    @Column(name = "nombre_new")
    private String nombreNew;
    @Size(max = 20)
    @Column(name = "primer_apellido_new")
    private String primerApellidoNew;
    @Size(max = 20)
    @Column(name = "segundo_apellido_new")
    private String segundoApellidoNew;
    @Size(max = 16)
    @Column(name = "telefono_new")
    private String telefonoNew;
    @Size(max = 45)
    @Column(name = "direccion_new")
    private String direccionNew;
    @Size(max = 50)
    @Column(name = "email_new")
    private String emailNew;
    @Size(max = 256)
    @Column(name = "password_new")
    private String passwordNew;
    @Column(name = "fecha_nacimiento_new")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimientoNew;
    @Size(max = 45)
    @Column(name = "razon_social_new")
    private String razonSocialNew;
    @Column(name = "id_ciudad_new")
    private Integer idCiudadNew;
    @Column(name = "id_departamento_new")
    private Integer idDepartamentoNew;

    public AuditoriaUsuarios() {
    }

    public AuditoriaUsuarios(Integer idAuditoriaUsuario) {
        this.idAuditoriaUsuario = idAuditoriaUsuario;
    }

    public AuditoriaUsuarios(Integer idAuditoriaUsuario, Date fecha) {
        this.idAuditoriaUsuario = idAuditoriaUsuario;
        this.fecha = fecha;
    }

    public Integer getIdAuditoriaUsuario() {
        return idAuditoriaUsuario;
    }

    public void setIdAuditoriaUsuario(Integer idAuditoriaUsuario) {
        this.idAuditoriaUsuario = idAuditoriaUsuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNombreOld() {
        return nombreOld;
    }

    public void setNombreOld(String nombreOld) {
        this.nombreOld = nombreOld;
    }

    public String getPrimerApellidoOld() {
        return primerApellidoOld;
    }

    public void setPrimerApellidoOld(String primerApellidoOld) {
        this.primerApellidoOld = primerApellidoOld;
    }

    public String getSegundoApellidoOld() {
        return segundoApellidoOld;
    }

    public void setSegundoApellidoOld(String segundoApellidoOld) {
        this.segundoApellidoOld = segundoApellidoOld;
    }

    public String getTelefonoOld() {
        return telefonoOld;
    }

    public void setTelefonoOld(String telefonoOld) {
        this.telefonoOld = telefonoOld;
    }

    public String getDireccionOld() {
        return direccionOld;
    }

    public void setDireccionOld(String direccionOld) {
        this.direccionOld = direccionOld;
    }

    public String getEmailOld() {
        return emailOld;
    }

    public void setEmailOld(String emailOld) {
        this.emailOld = emailOld;
    }

    public String getPasswordOld() {
        return passwordOld;
    }

    public void setPasswordOld(String passwordOld) {
        this.passwordOld = passwordOld;
    }

    public Date getFechaNacimientoOld() {
        return fechaNacimientoOld;
    }

    public void setFechaNacimientoOld(Date fechaNacimientoOld) {
        this.fechaNacimientoOld = fechaNacimientoOld;
    }

    public String getRazonSocialOld() {
        return razonSocialOld;
    }

    public void setRazonSocialOld(String razonSocialOld) {
        this.razonSocialOld = razonSocialOld;
    }

    public Integer getIdCiudadOld() {
        return idCiudadOld;
    }

    public void setIdCiudadOld(Integer idCiudadOld) {
        this.idCiudadOld = idCiudadOld;
    }

    public Integer getIdDepartamentoOld() {
        return idDepartamentoOld;
    }

    public void setIdDepartamentoOld(Integer idDepartamentoOld) {
        this.idDepartamentoOld = idDepartamentoOld;
    }

    public String getNombreNew() {
        return nombreNew;
    }

    public void setNombreNew(String nombreNew) {
        this.nombreNew = nombreNew;
    }

    public String getPrimerApellidoNew() {
        return primerApellidoNew;
    }

    public void setPrimerApellidoNew(String primerApellidoNew) {
        this.primerApellidoNew = primerApellidoNew;
    }

    public String getSegundoApellidoNew() {
        return segundoApellidoNew;
    }

    public void setSegundoApellidoNew(String segundoApellidoNew) {
        this.segundoApellidoNew = segundoApellidoNew;
    }

    public String getTelefonoNew() {
        return telefonoNew;
    }

    public void setTelefonoNew(String telefonoNew) {
        this.telefonoNew = telefonoNew;
    }

    public String getDireccionNew() {
        return direccionNew;
    }

    public void setDireccionNew(String direccionNew) {
        this.direccionNew = direccionNew;
    }

    public String getEmailNew() {
        return emailNew;
    }

    public void setEmailNew(String emailNew) {
        this.emailNew = emailNew;
    }

    public String getPasswordNew() {
        return passwordNew;
    }

    public void setPasswordNew(String passwordNew) {
        this.passwordNew = passwordNew;
    }

    public Date getFechaNacimientoNew() {
        return fechaNacimientoNew;
    }

    public void setFechaNacimientoNew(Date fechaNacimientoNew) {
        this.fechaNacimientoNew = fechaNacimientoNew;
    }

    public String getRazonSocialNew() {
        return razonSocialNew;
    }

    public void setRazonSocialNew(String razonSocialNew) {
        this.razonSocialNew = razonSocialNew;
    }

    public Integer getIdCiudadNew() {
        return idCiudadNew;
    }

    public void setIdCiudadNew(Integer idCiudadNew) {
        this.idCiudadNew = idCiudadNew;
    }

    public Integer getIdDepartamentoNew() {
        return idDepartamentoNew;
    }

    public void setIdDepartamentoNew(Integer idDepartamentoNew) {
        this.idDepartamentoNew = idDepartamentoNew;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAuditoriaUsuario != null ? idAuditoriaUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AuditoriaUsuarios)) {
            return false;
        }
        AuditoriaUsuarios other = (AuditoriaUsuarios) object;
        if ((this.idAuditoriaUsuario == null && other.idAuditoriaUsuario != null) || (this.idAuditoriaUsuario != null && !this.idAuditoriaUsuario.equals(other.idAuditoriaUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsi.proyectoSirec.jpa.entities.AuditoriaUsuarios[ idAuditoriaUsuario=" + idAuditoriaUsuario + " ]";
    }
    
}
