/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsi.proyectoSirec.jpa.sessions;

import com.adsi.proyectoSirec.jpa.entities.DetallePedidos;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author krix
 */
@Stateless
public class DetallePedidosFacade extends AbstractFacade<DetallePedidos> {
    @PersistenceContext(unitName = "com.mycompany_proyectoSirecBackend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DetallePedidosFacade() {
        super(DetallePedidos.class);
    }
    
      
    public List<DetallePedidos> findByIdUsuario(int idUsuario) {
        return getEntityManager().createNamedQuery("DetallePedidos.findByIdUsuario")
                .setParameter("idUsuario", idUsuario)
                .getResultList();
    }
    
     public DetallePedidos findByServicio(int idServicio) {
         try {
            return (DetallePedidos) getEntityManager().createNamedQuery("DetallePedidos.findByServicio")
                    .setParameter("idServicio", idServicio)
                    .getSingleResult();
        } catch (NonUniqueResultException ex) {
            throw ex;
        } catch (NoResultException ex) {
            return null;
        }
    }
}
