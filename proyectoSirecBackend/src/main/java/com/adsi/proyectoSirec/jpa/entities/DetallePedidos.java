/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsi.proyectoSirec.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lorena
 */
@Entity
@Table(name = "detalle_pedidos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetallePedidos.findAll", query = "SELECT d FROM DetallePedidos d"),
    @NamedQuery(name = "DetallePedidos.findByIdPedido", query = "SELECT d FROM DetallePedidos d WHERE d.detallePedidosPK.idPedido = :idPedido"),
    @NamedQuery(name = "DetallePedidos.findByIdServicio", query = "SELECT d FROM DetallePedidos d WHERE d.detallePedidosPK.idServicio = :idServicio"),
    @NamedQuery(name = "DetallePedidos.findByCantidad", query = "SELECT d FROM DetallePedidos d WHERE d.cantidad = :cantidad"),
    @NamedQuery(name = "DetallePedidos.findByFechaInicio", query = "SELECT d FROM DetallePedidos d WHERE d.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "DetallePedidos.findByFechaFin", query = "SELECT d FROM DetallePedidos d WHERE d.fechaFin = :fechaFin"),
    @NamedQuery(name = "DetallePedidos.findByPrecio", query = "SELECT d FROM DetallePedidos d WHERE d.precio = :precio"),
    @NamedQuery(name = "DetallePedidos.findByIdUsuario", query = "SELECT d FROM DetallePedidos d WHERE d.pedidos.idUsuario.idUsuario = :idUsuario"),
    @NamedQuery(name = "DetallePedidos.findByServicio", query = "SELECT d.servicios.nombre FROM DetallePedidos d WHERE (d.detallePedidosPK.idServicio = :idServicio) group by d.servicios.nombre")})
public class DetallePedidos implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DetallePedidosPK detallePedidosPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private int cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio")
    private double precio;
    @JoinColumn(name = "id_pedido", referencedColumnName = "id_pedido", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Pedidos pedidos;
    @JoinColumn(name = "id_servicio", referencedColumnName = "id_servicio", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Servicios servicios;

    public DetallePedidos() {
    }

    public DetallePedidos(DetallePedidosPK detallePedidosPK) {
        this.detallePedidosPK = detallePedidosPK;
    }

    public DetallePedidos(DetallePedidosPK detallePedidosPK, int cantidad, Date fechaInicio, Date fechaFin, double precio) {
        this.detallePedidosPK = detallePedidosPK;
        this.cantidad = cantidad;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.precio = precio;
    }

    public DetallePedidos(int idPedido, int idServicio) {
        this.detallePedidosPK = new DetallePedidosPK(idPedido, idServicio);
    }

    public DetallePedidosPK getDetallePedidosPK() {
        return detallePedidosPK;
    }

    public void setDetallePedidosPK(DetallePedidosPK detallePedidosPK) {
        this.detallePedidosPK = detallePedidosPK;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Pedidos getPedidos() {
        return pedidos;
    }

    public void setPedidos(Pedidos pedidos) {
        this.pedidos = pedidos;
    }

    public Servicios getServicios() {
        return servicios;
    }

    public void setServicios(Servicios servicios) {
        this.servicios = servicios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detallePedidosPK != null ? detallePedidosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetallePedidos)) {
            return false;
        }
        DetallePedidos other = (DetallePedidos) object;
        if ((this.detallePedidosPK == null && other.detallePedidosPK != null) || (this.detallePedidosPK != null && !this.detallePedidosPK.equals(other.detallePedidosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsi.proyectoSirec.jpa.entities.DetallePedidos[ detallePedidosPK=" + detallePedidosPK + " ]";
    }

}
