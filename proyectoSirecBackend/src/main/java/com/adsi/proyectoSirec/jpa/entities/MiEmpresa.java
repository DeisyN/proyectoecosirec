/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsi.proyectoSirec.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lorena
 */
@Entity
@Table(name = "mi_empresa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MiEmpresa.findAll", query = "SELECT m FROM MiEmpresa m"),
    @NamedQuery(name = "MiEmpresa.findByIdEmpresa", query = "SELECT m FROM MiEmpresa m WHERE m.idEmpresa = :idEmpresa"),
    @NamedQuery(name = "MiEmpresa.findByNombre", query = "SELECT m FROM MiEmpresa m WHERE m.nombre = :nombre"),
    @NamedQuery(name = "MiEmpresa.findByDireccion", query = "SELECT m FROM MiEmpresa m WHERE m.direccion = :direccion"),
    @NamedQuery(name = "MiEmpresa.findByTelefono", query = "SELECT m FROM MiEmpresa m WHERE m.telefono = :telefono"),
    @NamedQuery(name = "MiEmpresa.findByNit", query = "SELECT m FROM MiEmpresa m WHERE m.nit = :nit"),
    @NamedQuery(name = "MiEmpresa.findByRepresentateLegal", query = "SELECT m FROM MiEmpresa m WHERE m.representateLegal = :representateLegal")})
public class MiEmpresa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "direccion")
    private String direccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "telefono")
    private int telefono;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nit")
    private String nit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "representate_legal")
    private String representateLegal;
    @ManyToMany(mappedBy = "miEmpresaList")
    private List<ActividadesEconomicas> actividadesEconomicasList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Servicios> serviciosList;

    public MiEmpresa() {
    }

    public MiEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public MiEmpresa(Integer idEmpresa, String nombre, String direccion, int telefono, String nit, String representateLegal) {
        this.idEmpresa = idEmpresa;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.nit = nit;
        this.representateLegal = representateLegal;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getRepresentateLegal() {
        return representateLegal;
    }

    public void setRepresentateLegal(String representateLegal) {
        this.representateLegal = representateLegal;
    }

    @XmlTransient
    public List<ActividadesEconomicas> getActividadesEconomicasList() {
        return actividadesEconomicasList;
    }

    public void setActividadesEconomicasList(List<ActividadesEconomicas> actividadesEconomicasList) {
        this.actividadesEconomicasList = actividadesEconomicasList;
    }

    @XmlTransient
    public List<Servicios> getServiciosList() {
        return serviciosList;
    }

    public void setServiciosList(List<Servicios> serviciosList) {
        this.serviciosList = serviciosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmpresa != null ? idEmpresa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiEmpresa)) {
            return false;
        }
        MiEmpresa other = (MiEmpresa) object;
        if ((this.idEmpresa == null && other.idEmpresa != null) || (this.idEmpresa != null && !this.idEmpresa.equals(other.idEmpresa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsi.proyectoSirec.jpa.entities.MiEmpresa[ idEmpresa=" + idEmpresa + " ]";
    }
    
}
