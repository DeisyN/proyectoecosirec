
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.Servicios;
import com.adsi.proyectoSirec.jpa.sessions.ServiciosFacade;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("servicios")
public class ServiciosREST {
    
    @EJB
    private ServiciosFacade ejbServiciosFacade;
     
    @POST
    @RolesAllowed("ADMIN")
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Servicios servicios) {
        ejbServiciosFacade.create(servicios);
    }

    @PUT
    @RolesAllowed("ADMIN")
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, Servicios servicios) {
        ejbServiciosFacade.edit(servicios);
    }
    
    @PUT
    @RolesAllowed("ADMIN")
    @Path("inhabilitar/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
     public Response inhabilit(@PathParam("id") Integer id) {
         Servicios servicios=ejbServiciosFacade.find(id);
         if(servicios.isEstado()){
             servicios.setEstado(Boolean.FALSE);
             
         }else{
             servicios.setEstado(Boolean.TRUE);
         }
         
          GsonBuilder gsonBuilder = new GsonBuilder();
         Gson gson = gsonBuilder.create();
         
         try{
             ejbServiciosFacade.edit(servicios);
             return Response.ok()
                     .entity(gson.toJson("El estado del servicio se cambio satisfactoriamente"))
                     .build();
         }catch(Exception ex){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
         }
                 
     }

    @DELETE
    @RolesAllowed("ADMIN")
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbServiciosFacade.remove(ejbServiciosFacade.find(id));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Servicios> findAll() {
        return ejbServiciosFacade.findAll();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Servicios findById(@PathParam("id") Integer id) {
        return ejbServiciosFacade.find(id);
    }
    
}
