
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.Departamentos;
import com.adsi.proyectoSirec.jpa.sessions.DepartamentosFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("departamentos")
public class DepartamentosREST {
    
    @EJB
   private DepartamentosFacade ejbDepartamentosFacade;

   
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Departamentos> findAll(){
       return ejbDepartamentosFacade.findAll();
    }
   
    @GET
    @Path("nombre/{nombreDepartamento}")
    @Produces(MediaType.APPLICATION_JSON)
    public Departamentos findByNombre(@PathParam("nombreDepartamento") String nombreDepartamento) {
        return ejbDepartamentosFacade.find(nombreDepartamento);
    }
    
}
