package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.Roles;
import com.adsi.proyectoSirec.jpa.entities.Usuarios;
import com.adsi.proyectoSirec.jpa.sessions.UsuariosFacade;
import com.adsi.proyectoSirec.rest.auth.DigestUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("usuarios")
public class UsuariosREST {

    @EJB
    private UsuariosFacade ejbUsuariosFacade;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Usuarios usuario) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String pass = usuario.getPassword();
        if (ejbUsuariosFacade.findByEmail(usuario.getEmail()) == null) {
            if (usuario.getIdTipoUsuario().getIdTipoUsuario() == 1) {
                usuario.setIdRol(new Roles("USER"));
            } else {
                usuario.setIdRol(new Roles("EMP"));
                //aqui va la validacion del nit
                if (ejbUsuariosFacade.findByRazonSocial(usuario.getRazonSocial()) != null) {

                    return Response
                            .status(Response.Status.CONFLICT)
                            .entity(gson.toJson("El NIT ya se encuentra registrado"))
                            .build();
                }
            }
            try {
                usuario.setPassword(DigestUtil.generateDigest(pass));
            } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
                Logger.getLogger(UsuariosREST.class.getName()).log(Level.SEVERE, null, ex);
            }
            ejbUsuariosFacade.create(usuario);
            return Response.ok()
                    .entity(gson.toJson("El usuario fue creado exitosamente"))
                    .build();
        } else {
            return Response
                    .status(Response.Status.CONFLICT)
                    .entity(gson.toJson("El email ya se encuentra registrado"))
                    .build();
        }
    }

    @PUT
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, Usuarios usuario) {
        ejbUsuariosFacade.edit(usuario);
    }

    //para actualizar password
    @PUT
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("updatepass/{id}/{passOld}/{passNew}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updatepassword(@PathParam("id") Integer id, @PathParam("passOld") String passOld,
            @PathParam("passNew") String passNew) {
        
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        
        //Find User by Id            
        Usuarios user = ejbUsuariosFacade.find(id);
        
        System.out.println(compararPassword(user, passOld));
        
        if (compararPassword(user, passOld)) {
            System.out.println("HOLA, CONTRASEÑAS COINCIDEN");
            try {
                //Cifrar Password
                String nueva = DigestUtil.generateDigest(passNew);

                user.setPassword(nueva);
                
                //Edit User
                ejbUsuariosFacade.edit(user);
                return Response.ok()
                    .entity(gson.toJson("CONTRASEÑAS COINCIDEN, ACTUALIZADA CORRECTAMENTE"))
                    .build();
            } catch (Exception e) {
                Logger.getLogger(UsuariosREST.class.getName()).log(Level.SEVERE, null, e);
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity(gson.toJson("CONTRASEÑAS NO COINCIDEN, ERROR AL ACTUALIZAR"))
                        .build();
            }

        } else {
            System.out.println("LAS CONTRASEÑAS NO COINCIDEN");
            return Response.status(Response.Status.BAD_REQUEST)
                        .entity(gson.toJson("CONTRASEÑAS NO COINCIDEN, ERROR AL ACTUALIZAR"))
                        .build();
        }
    }
    private Boolean compararPassword(Usuarios user, String passAntigua) {

        try {
            String antigua = DigestUtil.generateDigest(passAntigua);
 
            if (antigua.equals(user.getPassword())) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(UsuariosREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    //fin de actualizar password
    
    //para confirmar contraseña para inhabilitar
    @POST
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("confirmarPassword/{id}/{passOld}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response confirmarPassword(@PathParam("id") Integer id, @PathParam("passOld") String passOld) {
        
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        
        //Find User by Id            
        Usuarios user = ejbUsuariosFacade.find(id);
        
        System.out.println(confirmarpass(user, passOld));
        
        if (confirmarpass(user, passOld)) {
            System.out.println("HOLA, CONTRASEÑAS COINCIDEN");
            try {
                //respuesta
                return Response.ok()
                    .entity(gson.toJson("CONTRASEÑA CORRECTA"))
                    .build();
            } catch (Exception e) {
                Logger.getLogger(UsuariosREST.class.getName()).log(Level.SEVERE, null, e);
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity(gson.toJson("CONTRASEÑA INCORRECTA"))
                        .build();
            }

        } else {
            System.out.println("CONTRASEÑA INCORRECTA");
            return Response.status(Response.Status.BAD_REQUEST)
                        .entity(gson.toJson("CONTRASEÑA INCORRECTA"))
                        .build();
        }

    }

    private Boolean confirmarpass(Usuarios user, String passAntigua) {

        try {
            String antigua = DigestUtil.generateDigest(passAntigua);
 
            if (antigua.equals(user.getPassword())) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(UsuariosREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }
    
    // fin de confirmar    

    // para actualizar la contraseña despues de la recuperacion
    
    @PUT
    @Path("actualizapass/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void editpass(@PathParam("id") Integer id, Usuarios usuario) {
        String newPassword = usuario.getPassword();
        usuario = ejbUsuariosFacade.find(id);
        try {
            usuario.setPassword(DigestUtil.generateDigest(newPassword));
            usuario.setCodigoRecuperacionPass(null);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex){
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
        }
        ejbUsuariosFacade.edit(usuario);
    }
    
    // fin de actualizar la contraseña despues de la recuperacion
    
    @DELETE
    @RolesAllowed("ADMIN")
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbUsuariosFacade.remove(ejbUsuariosFacade.find(id));
    }

    @GET
    @RolesAllowed("ADMIN")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Usuarios> findAll() {
        return ejbUsuariosFacade.findAll();
    }

    @PUT
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("inhabilitar/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response inhabilit(@PathParam("id") Integer id) {
        Usuarios usuario = ejbUsuariosFacade.find(id);

        if (usuario.isEstado()) {
            usuario.setEstado(Boolean.FALSE);
        } else {
            usuario.setEstado(Boolean.TRUE);
        }
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        try {
            ejbUsuariosFacade.edit(usuario);
            return Response.ok()
                    .entity(gson.toJson("El estado del usuario se actualizo satisfactoriamente"))
                    .build();
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
        }
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Usuarios findById(@PathParam("id") Integer id) {
        return ejbUsuariosFacade.find(id);
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("nombre/{nombre}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Usuarios> findByNombre(@PathParam("nombre") String nombre) {
        return ejbUsuariosFacade.findByNombre(nombre);
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("empresas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Usuarios> findByrol() {
        return ejbUsuariosFacade.findByRol("EMP");
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("user")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Usuarios> findByroluser() {
        return ejbUsuariosFacade.findByRol("USER");
    }

    @GET
    @Path("findByEmail/{email}")
    public Response findByEmail(@PathParam("email") String email) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        if (ejbUsuariosFacade.findByEmail(email) != null) {
            return Response
                    .status(Response.Status.CONFLICT)
                    .entity(gson.toJson("false"))
                    .build();
        } else {
            return Response.status(Response.Status.OK).entity(gson.toJson("true")).build();
        }
    }
    
    @GET
    @Path("findByNumIdent/{razonSocial}")
    public Response findByRazonSocial(@PathParam("razonSocial") String razonSocial) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        if (ejbUsuariosFacade.findByRazonSocial(razonSocial) != null) {
            return Response
                    .status(Response.Status.CONFLICT)
                    .entity(gson.toJson("false"))
                    .build();
        } else {
            return Response.status(Response.Status.OK).entity(gson.toJson("true")).build();
        }
    }
    
    @GET
    @Path("codigoRecuperacion/{idUsuario}/{codigoRecuperacionPass}")
    @Produces(MediaType.APPLICATION_JSON)
    public Usuarios findByIdAndCodigo(@PathParam("idUsuario") Integer idUsuario, @PathParam("codigoRecuperacionPass") String codigoRecuperacionPass) {
        return ejbUsuariosFacade.findByIdAndCodigo(idUsuario, codigoRecuperacionPass);
    }

}
