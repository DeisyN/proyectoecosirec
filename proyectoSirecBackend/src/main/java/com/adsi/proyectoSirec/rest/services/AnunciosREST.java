
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.Anuncios;
import com.adsi.proyectoSirec.jpa.entities.Usuarios;
import com.adsi.proyectoSirec.jpa.sessions.AnunciosFacade;
import com.adsi.proyectoSirec.rest.auth.AuthUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("anuncios")
public class AnunciosREST {
    
    @EJB
    private AnunciosFacade ejbAnunciosFacade;
    
    @Context
    private HttpServletRequest request;

    @POST
    @RolesAllowed({"ADMIN","EMP"})
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Anuncios anuncios) {
       GsonBuilder gsonBuilder = new GsonBuilder();
       Gson gson = gsonBuilder.create();
       try {
           anuncios.setIdUsuario(
           new Usuarios(
                Integer.parseInt(
                    AuthUtils.getSubject(
                        request.getHeader(
                            AuthUtils.AUTH_HEADER_KEY)))));
           ejbAnunciosFacade.create(anuncios);
           return Response.ok().entity(gson.toJson("El anuncio fue registrado exitosamente...")).build();
       } catch (ParseException | JOSEException | NumberFormatException ex) {
           Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
           return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
       }
        
    }

    @PUT
    @RolesAllowed({"ADMIN","EMP"})
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, Anuncios anuncios) {
        ejbAnunciosFacade.edit(anuncios);
    }
    
    @PUT
    @RolesAllowed({"ADMIN","EMP"})
    @Path("inhabilitar/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
     public Response inhabilit(@PathParam("id") Integer id) {
         Anuncios anuncios=ejbAnunciosFacade.find(id);
         if(anuncios.getEstado()){
             anuncios.setEstado(Boolean.FALSE);
             
         }else{
             anuncios.setEstado(Boolean.TRUE);
         }
         
          GsonBuilder gsonBuilder = new GsonBuilder();
         Gson gson = gsonBuilder.create();
         
         try{
             ejbAnunciosFacade.edit(anuncios);
             return Response.ok()
                     .entity(gson.toJson("El estado del anuncio se cambio satisfactoriamente"))
                     .build();
         }catch(Exception ex){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
         }
                 
     }

    @DELETE
    @RolesAllowed("ADMIN")
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbAnunciosFacade.remove(ejbAnunciosFacade.find(id));
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Produces(MediaType.APPLICATION_JSON)
    public List<Anuncios> findAll() {
        return ejbAnunciosFacade.findAll();
    }


    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Anuncios findById(@PathParam("id") Integer id) {
        return ejbAnunciosFacade.find(id);
    }
    
    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("usuario")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Anuncios> findByIdUsuario(){
             try {
            return ejbAnunciosFacade.findByIdUsuario(Integer.parseInt(
                    AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY))));
        } catch (ParseException | JOSEException ex) {
            return null;
        }

    }
    
}
