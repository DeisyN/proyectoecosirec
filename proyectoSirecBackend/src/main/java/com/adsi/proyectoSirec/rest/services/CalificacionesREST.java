
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.Calificaciones;
import com.adsi.proyectoSirec.jpa.entities.Usuarios;
import com.adsi.proyectoSirec.jpa.sessions.CalificacionesFacade;
import com.adsi.proyectoSirec.rest.auth.AuthUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("calificaciones")
public class CalificacionesREST {
    
    @EJB
    private CalificacionesFacade ejbCalificacionesFacade;
    
    @Context
    private HttpServletRequest request;

    @POST
    @RolesAllowed({"ADMIN", "USER"})
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Calificaciones calificaciones) {
       GsonBuilder gsonBuilder = new GsonBuilder();
       Gson gson = gsonBuilder.create();
       try {
           calificaciones.setIdUsuario(
           new Usuarios(
                Integer.parseInt(
                    AuthUtils.getSubject(
                        request.getHeader(
                            AuthUtils.AUTH_HEADER_KEY)))));
           ejbCalificacionesFacade.create(calificaciones);
           return Response.ok().entity(gson.toJson("La calificacion fue registrada exitosamente")).build();
       } catch (ParseException | JOSEException | NumberFormatException ex) {
           Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
           return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
       }
        
    }
    

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Produces(MediaType.APPLICATION_JSON)
    public List<Calificaciones> findAll() {
        return ejbCalificacionesFacade.findAll();
    }
    
    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Calificaciones findById(@PathParam("id") Integer id) {
        return ejbCalificacionesFacade.find(id);
    }
    
    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Produces(MediaType.APPLICATION_JSON)
    @Path("total/{idEmpresa}")
    public List<Calificaciones> findByIdEmpresaCalificaciones(@PathParam("idEmpresa") Integer idEmpresa) {
        //Usuarios usuarios = ejbUsuariosFacade.find(id);
        return ejbCalificacionesFacade.findByIdEmpresaCalificaciones(idEmpresa);
    }
}
