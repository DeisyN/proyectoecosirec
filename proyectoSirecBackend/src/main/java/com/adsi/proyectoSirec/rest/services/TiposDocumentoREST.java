
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.TiposDocumento;
import com.adsi.proyectoSirec.jpa.sessions.TiposDocumentoFacade;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("tiposDocumento")
public class TiposDocumentoREST {
    
    @EJB
    private TiposDocumentoFacade ejbTiposDocumentoFacade;
   
    @POST
    @RolesAllowed("ADMIN")
    @Consumes(MediaType.APPLICATION_XML)
    public void create(TiposDocumento tipoDocumento){
       ejbTiposDocumentoFacade.create(tipoDocumento);
    }
   
    @PUT
    @RolesAllowed("ADMIN")
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") String id, TiposDocumento tiposDocumento) {
        ejbTiposDocumentoFacade.edit(tiposDocumento);
    }

    @DELETE
    @RolesAllowed("ADMIN")
    @Path("{id}")
    public void remove(@PathParam("id") String id) {
        ejbTiposDocumentoFacade.remove(ejbTiposDocumentoFacade.find(id));
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Produces(MediaType.APPLICATION_JSON)
    public List<TiposDocumento> findAll() {
        return ejbTiposDocumentoFacade.findAll();
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public TiposDocumento findById(@PathParam("id") String id) {
        return ejbTiposDocumentoFacade.find(id);
    }
    
}
