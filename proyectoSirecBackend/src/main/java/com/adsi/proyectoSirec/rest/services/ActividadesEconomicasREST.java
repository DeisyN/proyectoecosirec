
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.ActividadesEconomicas;
import com.adsi.proyectoSirec.jpa.sessions.ActividadesEconomicasFacade;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("actividadesEconomicas")
public class ActividadesEconomicasREST {
    
    @EJB
    private ActividadesEconomicasFacade ejbActividadesEconomicasFacade;
    
    @POST
    @RolesAllowed("ADMIN")
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(ActividadesEconomicas actividadesEconomicas) {
       ejbActividadesEconomicasFacade.create(actividadesEconomicas);
    }

    @PUT
    @RolesAllowed("ADMIN")
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, ActividadesEconomicas actividadesEconomicas) {
        ejbActividadesEconomicasFacade.edit(actividadesEconomicas);
    }

    @DELETE
    @RolesAllowed("ADMIN")
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbActividadesEconomicasFacade.remove(ejbActividadesEconomicasFacade.find(id));
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Produces(MediaType.APPLICATION_JSON)
    public List<ActividadesEconomicas> findAll() {
        return ejbActividadesEconomicasFacade.findAll();
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public ActividadesEconomicas findById(@PathParam("id") Integer id) {
        return ejbActividadesEconomicasFacade.find(id);
    }
    
}

