/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.RecuperarPassword;
import com.adsi.proyectoSirec.jpa.entities.Usuarios;
import com.adsi.proyectoSirec.jpa.sessions.UsuariosFacade;
import com.adsi.proyectoSirec.rest.auth.DigestUtil;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

/**
 *
 * @author lorena
 */
@Path("recuperarPass")
public class RecuperarPasswordRest {
    @EJB
    private UsuariosFacade ejbUsuariosFacade;
    
    @POST
    @Path("recuperarpassword")
    @Consumes(MediaType.APPLICATION_JSON)
    public void recuperarPass(RecuperarPassword recuperarPassword) {
        try {

            Usuarios user = ejbUsuariosFacade.findByEmail(recuperarPassword.getEmail());
            int x = (int) (Math.random() * 1000);

            String cd = null;
            try {
                cd = DigestUtil.generateDigest(String.valueOf(x));
            } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
                Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            }

            HtmlEmail email = new HtmlEmail();

            email.setHostName("smtp.gmail.com");
            email.setSmtpPort(465);
            email.setAuthenticator(new DefaultAuthenticator("ecosirec@gmail.com", "adsi934096"));
            email.setSSL(true);
            email.setFrom("ecosirec@gmail.com");
            email.setSubject("Restablecer Contraseña Ecosirec");
            email.setHtmlMsg("<div style=\"border-bottom:1px solid; text-align:center; background:#3c948b; display:inline-block; width:100%\"><img src=\"https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-xlp1/v/t1.0-9/13102712_219740741750573_8163027518940312721_n.jpg?oh=30e35b812f0abef37e0426be798befb8&oe=58557E39&__gda__=1485158699_cb1391adbe9ef30fdde5c187ed0a867d\" alt=\"ECOSIREC\" style=\"width:6em; border-radius:50%; margin-top:.5em\"><h1 style=\"margin:0; padding:0.5em 0.5em 0.5em 0; color:white; font-family:sans-serif; width:80%; display:inline-block; float:right; text-align:right\">Restablecer Contraseña</h1></div><div style=\"\">\n"
                    + "          <p style=\"padding-left:2em; margin:2em 0 2em 0; font-family:sans-serif\">Abra el siguiente enlace para continuar con el proceso de recuperación <a href=\"http://localhost:8001/#/nuevaPassword/" + user.getIdUsuario()+ "/" + cd + "\">http://localhost:8001/#/nuevaPassword/" + user.getIdUsuario()+ "/" + cd + "</a></p>\n"
                    + "      \n"
                    + "        </div>");
            
            email.addTo(recuperarPassword.getEmail());
            
            user.setCodigoRecuperacionPass(cd);
            
            ejbUsuariosFacade.edit(user);
            
            email.send();
        } catch (EmailException ex) {
            Logger.getLogger(RecuperarPasswordRest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }


}
