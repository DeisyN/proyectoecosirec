
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.Ciudades;
import com.adsi.proyectoSirec.jpa.sessions.CiudadesFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("ciudades")
public class CiudadesREST {
    
    @EJB
    private CiudadesFacade ejbCiudadesFacade;
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Ciudades> findAll(){
        return ejbCiudadesFacade.findAll();
    }
    

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("nombre/{query}")
    public List<Ciudades> findByNombre(@PathParam("query") String query) {
        return ejbCiudadesFacade.findByNombre(query);
    }
    
}
