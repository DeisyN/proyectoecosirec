
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.Estados;
import com.adsi.proyectoSirec.jpa.entities.Pedidos;
import com.adsi.proyectoSirec.jpa.entities.Usuarios;
import com.adsi.proyectoSirec.jpa.sessions.PedidosFacade;
import com.adsi.proyectoSirec.rest.auth.AuthUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("pedidos")
public class PedidosREST {
    
    @EJB
    private PedidosFacade ejbPedidosFacade;
    
    
    @Context
    private HttpServletRequest request;

    @POST
    @RolesAllowed("EMP")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Pedidos pedidos) {
       GsonBuilder gsonBuilder = new GsonBuilder();
       Gson gson = gsonBuilder.create();
       try {
           pedidos.setIdEstado(new Estados(3));
           pedidos.setIdUsuario(
           new Usuarios(
                Integer.parseInt(
                    AuthUtils.getSubject(
                        request.getHeader(
                            AuthUtils.AUTH_HEADER_KEY)))));
           
           ejbPedidosFacade.create(pedidos);
    
           return Response.ok().entity(gson.toJson(pedidos)).build();
       } catch (ParseException | JOSEException | NumberFormatException ex) {
           Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
           return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
       }
        
    }  
    
    @PUT
    @RolesAllowed({"ADMIN", "EMP"})
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, Pedidos pedidos) {
        ejbPedidosFacade.edit(pedidos);
    }
    
        
    /*@PUT
    @Path("inhabilitar/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
     public Response inhabilit(@PathParam("id") Integer id) {
         Pedidos pedidos = ejbPedidosFacade.find(id);
         if(pedidos.isEstado()){
             pedidos.setEstado(Boolean.FALSE);
             
         }else{
             pedidos.setEstado(Boolean.TRUE);
         }
         
          GsonBuilder gsonBuilder = new GsonBuilder();
         Gson gson = gsonBuilder.create();
         
         try{
             ejbPedidosFacade.edit(pedidos);
             return Response.ok()
                     .entity(gson.toJson("El estado del pedido se cambio satisfactoriamente"))
                     .build();
         }catch(Exception ex){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
         }
                 
     }*/

    @DELETE
    @RolesAllowed("ADMIN")
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbPedidosFacade.remove(ejbPedidosFacade.find(id));
    }

    @GET
    @RolesAllowed({"ADMIN", "EMP"})
    @Produces(MediaType.APPLICATION_JSON)
    public List<Pedidos> findAll() {
        return ejbPedidosFacade.findAll();
    }

    @GET
    @RolesAllowed({"ADMIN", "EMP"})
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Pedidos findById(@PathParam("id") Integer id) {
        return ejbPedidosFacade.find(id);
    }
    
}
