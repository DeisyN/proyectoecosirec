
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.MiEmpresa;
import com.adsi.proyectoSirec.jpa.sessions.MiEmpresaFacade;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("miEmpresa")
public class MiEmpresaREST {
    
    @EJB
    private MiEmpresaFacade ejbMiEmpresaFacade;

    @POST
    @RolesAllowed("ADMIN")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(MiEmpresa miEmpresa) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        if (ejbMiEmpresaFacade.findByNit(miEmpresa.getNit()) == null) {
            ejbMiEmpresaFacade.create(miEmpresa);
            return Response.ok()
                    .entity(gson.toJson("Esta empresa fue creada exitosamente"))
                    .build();
        } else {
            return Response
                    .status(Response.Status.CONFLICT)
                    .entity(gson.toJson("El nit de La empresa ya esta registrado"))
                    .build();
        }

    }

    @PUT
    @RolesAllowed("ADMIN")
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, MiEmpresa miEmpresa) {
        ejbMiEmpresaFacade.edit(miEmpresa);
    }

    @DELETE
    @RolesAllowed("ADMIN")
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbMiEmpresaFacade.remove(ejbMiEmpresaFacade.find(id));
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Produces(MediaType.APPLICATION_JSON)
    public List<MiEmpresa> findAll() {
        return ejbMiEmpresaFacade.findAll();
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public MiEmpresa findById(@PathParam("id") Integer id) {
        return ejbMiEmpresaFacade.find(id);
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("nombre/{nombre}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<MiEmpresa> findByNombre(@PathParam("nombre") String nombre) {
        return ejbMiEmpresaFacade.findByNombre(nombre);
    }
    
    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("nit/{nit}")
    @Produces(MediaType.APPLICATION_JSON)
    public MiEmpresa findByNitConsulta(@PathParam("nit") String nit) {
        return ejbMiEmpresaFacade.find(nit);
    }
}
