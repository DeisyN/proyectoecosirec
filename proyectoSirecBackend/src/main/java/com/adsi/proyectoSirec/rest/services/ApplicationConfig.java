
package com.adsi.proyectoSirec.rest.services;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

@ApplicationPath("webresources")
public class ApplicationConfig extends ResourceConfig {

    public ApplicationConfig() {
        packages("com.adsi.proyectoSirec.rest.services; com.adsi.proyectoSirec.rest.auth");
        register(RolesAllowedDynamicFeature.class);  
    }
    
}
