/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.Email;
import com.adsi.proyectoSirec.jpa.entities.Usuarios;
import com.adsi.proyectoSirec.jpa.sessions.EmailFacade;
import com.adsi.proyectoSirec.rest.auth.AuthUtils;
import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("contactar")
public class ContactarREST {
    @EJB
    private com.adsi.proyectoSirec.jpa.sessions.UsuariosFacade ejbFacade;
    @EJB
    private EmailFacade emailFacade;
    @Context
    private HttpServletRequest request;
     
    @POST
    @RolesAllowed("USER")
    @Consumes(MediaType.APPLICATION_JSON)
    public void enviarEmail(Email email) {
        try {
            Usuarios user = ejbFacade.find(Integer.parseInt(
                    AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY)
                    )
            ));
            emailFacade.sendMail(email.getEmailEmpresa(), user.getEmail(), "Solicitud de Contacto", email.getTextoMensaje());
        } catch (ParseException | JOSEException | NamingException | MessagingException ex) {
            Logger.getLogger(ContactarREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

/*
   @POST
   @Consumes("application/x-www-form-urlencoded")
   public void enviarEmail(@FormParam("emailEmpresa") String emailEmpresa, @FormParam("textoMensaje") String textoMensaje){
        try {
            Usuarios user = ejbFacade.find(Integer.parseInt(
                    AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY)
                    )
            ));
            emailFacade.sendMail(emailEmpresa, user.getEmail(), "Solicitud de Contacto", textoMensaje);
        } catch (ParseException | JOSEException | NamingException | MessagingException ex) {
            Logger.getLogger(ContactarREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }   
*/
    
}