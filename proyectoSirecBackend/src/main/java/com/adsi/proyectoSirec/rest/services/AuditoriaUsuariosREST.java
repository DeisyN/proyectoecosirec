
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.AuditoriaUsuarios;
import com.adsi.proyectoSirec.jpa.sessions.AuditoriaUsuariosFacade;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("auditoriaUsuarios")
public class AuditoriaUsuariosREST {
    
    @EJB
    private AuditoriaUsuariosFacade ejbAuditoriaUsuariosFacade;
    
    @GET
    @RolesAllowed("ADMIN")
    @Produces(MediaType.APPLICATION_JSON)
    public List<AuditoriaUsuarios> findAll() {
        return ejbAuditoriaUsuariosFacade.findAll();
    }

    @GET
    @RolesAllowed("ADMIN")
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public AuditoriaUsuarios findById(@PathParam("id") Integer id) {
        return ejbAuditoriaUsuariosFacade.find(id);
    }
}
