
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.TipoUsuarios;
import com.adsi.proyectoSirec.jpa.sessions.TipoUsuariosFacade;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("tipoUsuarios")
public class TipoUsuariosREST {
    
    @EJB
    private TipoUsuariosFacade ejbTipoUsuariosFacade;
    
    @POST
    @RolesAllowed("ADMIN")
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(TipoUsuarios tipoUsuario){
        ejbTipoUsuariosFacade.create(tipoUsuario);
    }
    
    @PUT
    @RolesAllowed("ADMIN")
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, TipoUsuarios tipoUsuario){
        ejbTipoUsuariosFacade.edit(tipoUsuario);
    }
    
    @DELETE
    @RolesAllowed("ADMIN")
    @Path("{id}")
    public void remove(@PathParam("id") Integer id){
        ejbTipoUsuariosFacade.remove(ejbTipoUsuariosFacade.find(id));
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TipoUsuarios> findAll(){
        return ejbTipoUsuariosFacade.findAll();
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public TipoUsuarios findById(@PathParam("id") Integer id) {
        return ejbTipoUsuariosFacade.find(id);
    }
    
}
