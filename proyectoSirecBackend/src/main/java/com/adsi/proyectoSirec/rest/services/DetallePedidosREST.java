
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.DetallePedidos;
import com.adsi.proyectoSirec.jpa.entities.Usuarios;
import com.adsi.proyectoSirec.jpa.sessions.DetallePedidosFacade;
import com.adsi.proyectoSirec.jpa.sessions.EmailFacade;
import com.adsi.proyectoSirec.jpa.sessions.PedidosFacade;
import com.adsi.proyectoSirec.jpa.sessions.ServiciosFacade;
import com.adsi.proyectoSirec.rest.auth.AuthUtils;
import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.persistence.Transient;

@Path("detallePedidos")
public class DetallePedidosREST {
    
    @EJB
    private DetallePedidosFacade ejbDetallePedidosFacade;
    
    @EJB
    private PedidosFacade ejbPedidosFacade;
    
    @EJB
    private ServiciosFacade ejbServiciosFacade;
    
    @EJB
    private EmailFacade emailFacade;
    
    @EJB
    private com.adsi.proyectoSirec.jpa.sessions.UsuariosFacade ejbFacade;
    
    @Transient
    SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy");
    
    @Context
    private HttpServletRequest request;


    @POST
    @RolesAllowed("EMP")
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(DetallePedidos detallePedidos) {
        ejbDetallePedidosFacade.create(detallePedidos);
        
         try {
            Usuarios user = ejbFacade.find(Integer.parseInt(
                    AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY)
                    )
            ));
            emailFacade.sendMailHtml(user.getEmail(), "Informe de su pedido", "<!DOCTYPE html>\n"
                    + "<html lang=\"es\">\n"
                    + "<head>\n"
                    + "  <meta charset=\"UTF-8\">\n"
                    + "  <title>Información de su pedido</title>\n"
                    + "</head>\n"
                    + "<body>\n"
                    + "  <div class=\"Email-container\" style=\" /*display: flex;*/ flex-direction: column; max-width: 50em; margin: auto; width: 95%;\">\n"
                    + "    <div style=\"background: #3c948b;\">\n"
                    + "      <h1 class=\"Tittle\" style=\"border: none; /*font-size:1.5em;*/ padding: .5em 1em; color:white; text-align: center;\">Ecosirec</h1>\n"
                    + "    </div>\n"
                    + "    <p style=\" color: black; \">Esta es una factura de pago de servicio que usted solicito</p>\n"
                    + "    <div style=\"margin: auto; width: 90%;\">\n"
                    + "      <div class=\"Usuario\"  style=\"background:#3c948b; color: white; padding: 1em; text-align:center;\">\n"
                    + "        <p class=\"Parrafos\" style=\"margin: 0;\">Información de tu pedido</p>\n"
                    + "      </div>\n"
                    + "      <div class=\"Usuario\"  style=\" border-bottom: 1px solid #C7C7C7; padding: .5em 1em; color: black;\">\n"
                    + "        <p class=\"Parrafos\" style=\" position: relative; margin: 0;\">N° Pedido: <span style=\" position:absolute; left: 35%;\">" + detallePedidos.getDetallePedidosPK().getIdPedido() + detallePedidos.getDetallePedidosPK().getIdServicio() + "</span></p>\n"
                    + "      </div>\n"
                    + "      <div class=\"Usuario\"  style=\" border-bottom: 1px solid #C7C7C7; padding: .5em 1em; color: black;\">\n"
                    + "        <p class=\"Parrafos\" style=\" position: relative; margin: 0;\">Usuario: <span style=\" position:absolute; left: 35%;\">"  + user.getNombre() + "</span></p>\n"
                    + "      </div>\n"
                    + "      <div class=\"Paquete\"  style=\" border-bottom: 1px solid #C7C7C7; padding: .5em 1em; color: black;\">\n"
                    + "        <p class=\"Parrafos\" style=\" position: relative; margin: 0;\"> Paquete: <span style=\" position:absolute; left: 35%;\">" + detallePedidos.getDetallePedidosPK().getIdServicio() + "</span></p>\n"
                    + "      </div>\n"
                    + "      <div class=\"Fechas-ini\"  style=\" border-bottom: 1px solid #C7C7C7; padding: .5em 1em; color: black;\">\n"
                    + "        <p class=\"Parrafos\" style=\" position: relative; margin: 0;\">Fecha incio:  <span style=\" position:absolute; left: 35%;\">" + date.format(detallePedidos.getFechaInicio()) + "</span></p>\n"
                    + "      </div>\n"
                    + "      <div class=\"Fechas-fin\"  style=\"border-bottom: 1px solid #C7C7C7; padding: .5em 1em; color: black;\">\n"
                    + "        <p class=\"Parrafos\" style=\" position: relative; margin: 0;\">Fecha fin: <span style=\"position:absolute; left: 35%;\">" + date.format(detallePedidos.getFechaFin()) + "</span></p>\n"
                    + "      </div>\n"
                    + "      <div class=\"Cantidad\"  style=\" border-bottom: 1px solid #C7C7C7; padding: .5em 1em; color: black;\">\n"
                    + "        <p class=\"Parrafos\" style=\" position: relative; margin: 0;\"> Cantidad: <span style=\" position:absolute; left: 35%;\">" + detallePedidos.getCantidad() + "</span></p>\n"
                    + "      </div>\n"
                    + "      <div class=\"Cantidad\"  style=\" border-bottom: 1px solid #C7C7C7; padding: .5em 1em; color: black;\">\n"
                    + "        <p class=\"Parrafos\" style=\" position: relative; margin: 0;\"> Valor unitario: <span style=\" position:absolute; left: 35%;\">" + detallePedidos.getPrecio() + "</span></p>\n"
                    + "      </div>\n"
                    + "      <div class=\"Total\"  style=\" border-bottom: 1px solid #C7C7C7; border-spacing: 2px; padding: .5em 1em; color: black;\">\n"
                    + "        <p class=\"Parrafos\" style=\" position: relative; margin: 0;\">Total a pagar: <span style=\" position:absolute; left: 35%;\">" + (detallePedidos.getPrecio()*detallePedidos.getCantidad()) + "</span></p>\n"
                    + "      </div>\n"
                    + "    </div>\n"
                    + "    <div style=\" background: #3c948b; margin-top:1.5em;\">\n"
                    + "      <h2 class=\"Tittle\" style=\" border: none; padding: .5em 1em; color:white; text-align: center;\">Para más información ingrese a <a href=\"http://frontend-ecosirec.rhcloud.com\"  target=\"_blank\">www.Ecosirec.com</a></h2>\n"
                    + "    </div>\n"
                    + "  </div>\n"
                    + "</body>\n"
                    + "</html>");
        } catch (ParseException | JOSEException | NamingException | MessagingException ex) {
            Logger.getLogger(DetallePedidosREST.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    }//detallePedidos.getDetallePedidosPK().getIdServicio()

   
    @GET
    @RolesAllowed({"EMP", "ADMIN"})
    @Produces(MediaType.APPLICATION_JSON)
    public List<DetallePedidos> findAll() {
        return ejbDetallePedidosFacade.findAll();
    }
    
    @GET
    @RolesAllowed({"EMP", "ADMIN"})
    @Path("usuario")
    @Produces(MediaType.APPLICATION_JSON)
    public List<DetallePedidos> findByIdUsuario(){
             try {
            return ejbDetallePedidosFacade.findByIdUsuario(Integer.parseInt(
                    AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY))));
        } catch (ParseException | JOSEException ex) {
            return null;
        }

    }
    
    
}
