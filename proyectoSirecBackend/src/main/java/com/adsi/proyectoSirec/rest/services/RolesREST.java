
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.Roles;
import com.adsi.proyectoSirec.jpa.sessions.RolesFacade;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("roles")
public class RolesREST {
    
    @EJB
    private RolesFacade ejbRolesFacade;

    @POST
    @RolesAllowed("ADMIN")
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Roles rol) {
        ejbRolesFacade.create(rol);
    }

    @PUT
    @RolesAllowed("ADMIN")
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") String id, Roles rol) {
        ejbRolesFacade.edit(rol);
    }

    @DELETE
    @RolesAllowed("ADMIN")
    @Path("{id}")
    public void remove(@PathParam("id") String id) {
        ejbRolesFacade.remove(ejbRolesFacade.find(id));
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Produces(MediaType.APPLICATION_JSON)
    public List<Roles> findAll() {
        return ejbRolesFacade.findAll();
    }

    @GET
    @RolesAllowed({"ADMIN", "USER", "EMP"})
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Roles findById(@PathParam("id") String id) {
        return ejbRolesFacade.find(id);
    }
}
