
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.Paises;
import com.adsi.proyectoSirec.jpa.sessions.PaisesFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("paises")
public class PaisesREST {
    
    @EJB
    private PaisesFacade ejbPaisesFacade;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Paises> finAll(){
        return ejbPaisesFacade.findAll();
    }
    
    @GET
    @Path("nombre/{nombrePais}")
    @Produces(MediaType.APPLICATION_JSON)
    public Paises findByNombre(@PathParam("nombrePais") String nombrePais) {
        return ejbPaisesFacade.find(nombrePais);
    }
    
}
