/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsi.proyectoSirec.rest.services;

import com.adsi.proyectoSirec.jpa.entities.Estados;
import com.adsi.proyectoSirec.jpa.sessions.EstadosFacade;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author lorena
 */
@Path("estados")
public class EstadosREST {
    
    @EJB
    private EstadosFacade ejbEstadosFacade;
     
    @POST
    @RolesAllowed("ADMIN")
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Estados estados) {
        ejbEstadosFacade.create(estados);
    }

    @PUT
    @RolesAllowed("ADMIN")
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, Estados estados) {
        ejbEstadosFacade.edit(estados);
    }
    
    @DELETE
    @RolesAllowed("ADMIN")
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbEstadosFacade.remove(ejbEstadosFacade.find(id));
    }

    @GET
    @RolesAllowed({"ADMIN","EMP"})
    @Produces(MediaType.APPLICATION_JSON)
    public List<Estados> findAll() {
        return ejbEstadosFacade.findAll();
    }

    @GET
    @RolesAllowed({"ADMIN","EMP"})
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Estados findById(@PathParam("id") Integer id) {
        return ejbEstadosFacade.find(id);
    }
    
}
