(function() {
    'use strict';

    angular.module('app.portafolio.router', [
            'app.portafolio.controller'
        ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('portafolio', {
                url: '/portafolio',
                template: '<portafolio/>'
            }).state('inhabilitarportafolio', {
                url: '/portafolio',
                template: '<inhabilitarportafolio/>'
            }).state('portafolioupdate', {
                url: '/portafolio/update/:idServicio',
                template: '<portafolioupdate/>'
            }).state('portafoliocreate', {
                url: '/portafolio/create',
                template: '<portafoliocreate></portafoliocreate>'
            });
    }
})();
