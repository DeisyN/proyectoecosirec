(function() {
    'use strict';

    angular.module('app.portafolio.controller', [

        ]).controller('PortafolioListCtrl', PortafolioListCtrl)
        .controller('InhabilitarPortafolioCtrl', InhabilitarPortafolioCtrl)
        .controller('PortafolioUpdateCtrl', PortafolioUpdateCtrl)
        .controller('PortafoliocreateCtrl', PortafoliocreateCtrl);

    PortafolioListCtrl.$inject = ['Servicios', '$mdToast', 'SecurityFilter', '$location'];

    function PortafolioListCtrl(Servicios, $mdToast, SecurityFilter, $location) {
        var vm = this;
        vm.servicio = Servicios.query().$promise
            .then(function(data) {
                vm.servicio = data;

            });
    }

    InhabilitarPortafolioCtrl.$inject = ['Servicios', '$mdToast', 'SecurityFilter', '$location'];

    function InhabilitarPortafolioCtrl(Servicios, $mdToast, SecurityFilter, $location) {
        var vm = this;

        if (SecurityFilter.isAdmin()) {

            vm.inhabilitar = function(id) {

                Servicios.inhabilitar({
                        idServicio: id
                    }).$promise
                    .then(function(data) {
                        console.log("realizado con exito");
                        console.log(data);
                        $mdToast.show(
                            $mdToast.simple()
                            .textContent('Estado actualizado exitosamente...')
                            .position('bottom right'));
                    })
                    .catch(function(data) {
                        console.log("fallo en inhabilitar");
                        console.log(data);
                        $mdToast.show(
                            $mdToast.simple()
                            .textContent('Error al actualizar el estado...')
                            .position('bottom right'));
                    });
            };
        } else {
            $location.path('/');
        }
    }

    PortafolioUpdateCtrl.$inject = ['Servicios', '$stateParams', '$location', '$mdToast', 'SecurityFilter'];

    function PortafolioUpdateCtrl(Servicios, $stateParams, $location, $mdToast, SecurityFilter) {
        var vm = this;
        vm.mens = '';
        vm.img = false;
        if (SecurityFilter.isAdmin()) {
            vm.servicio = Servicios.get({
                idServicio: $stateParams.idServicio
            });
            // vm.loadImg = loadImg;

            vm.update = function() {
                vm.servicio.idEmpresa = {};
                vm.servicio.idEmpresa.idEmpresa = 1;
                Servicios.update(vm.servicio, function() {
                    $location.path('/portafolio');
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Se ha  actualizado el  Servicio para publicidad...')
                        .position('bottom right'));
                });
            };

            vm.loadImg =  function($fileContent) {
              vm.mens = '';
              vm.img = false;
              console.log($fileContent.length);
               if($fileContent.length > 2000){
                 console.log('HOLA DE TRUE');
                   vm.servicio.imagen = $fileContent;
               } else {
                 console.log('HOLA DE FALSE');
                 vm.mens = 'La imágen es muy grande!';
                 vm.img = true;
                 vm.servicio.imagen = null;
               }

             };

             vm.mensaje = function(){
               return vm.mens;
             };

             vm.imagen = function(){
               return vm.img;
             };

        } else {
            $location.path('/');
        }

    }

    PortafoliocreateCtrl.$inject = ['Servicios', '$location', '$mdToast', 'SecurityFilter'];

    function PortafoliocreateCtrl(Servicios, $location, $mdToast, SecurityFilter) {
        var vm = this;
        vm.mens = '';
        vm.img = false;

        if (SecurityFilter.isAdmin()) {
            // vm.loadImg = loadImg;
            vm.servicio = {};

            vm.create = function() {
                console.log(vm.servicio);
                vm.servicio.idEmpresa = {};
                vm.servicio.idEmpresa.idEmpresa = 1;
                Servicios.save(vm.servicio, function() {
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Sevicio fue creado exitosamente...')
                        .position('bottom right'));
                    $location.path('/portafolio');
                });

            };

           vm.loadImg =  function($fileContent) {
             vm.mens = '';
             vm.img = false;
             console.log($fileContent.length);
              if($fileContent.length > 2000){
                console.log('HOLA DE TRUE');
                  vm.servicio.imagen = $fileContent;
              } else {
                console.log('HOLA DE FALSE');
                vm.mens = 'La imágen es muy grande!';
                vm.img = true;
                vm.servicio.imagen = null;
              }

            };

            vm.mensaje = function(){
              return vm.mens;
            };

            vm.imagen = function(){
              return vm.img;
            };

        } else {
            $location.path('/');
        }
    }
})();
