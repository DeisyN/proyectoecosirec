(function() {
    'use strict';

    angular.module('app.portafolio.directivas', [

        ]).directive('portafolio', portafolio)
        .directive('portafolioupdate', portafolioupdate)
        .directive('inhabilitarportafolio', inhabilitarportafolio)
        .directive('portafoliocreate', portafoliocreate);

    function portafolio() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/portafolio/portafolio.html',
            controller: 'PortafolioListCtrl',
            controllerAs: 'vm'
        };
    }

    function inhabilitarportafolio() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/portafolio/portafolio.html',
            controller: 'InhabilitarPortafolioCtrl',
            controllerAs: 'vm'
        };
    }

    function portafolioupdate() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/portafolio/update.html',
            controller: 'PortafolioUpdateCtrl',
            controllerAs: 'vm'
        };
    }

    function portafoliocreate() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/portafolio/create.html',
            controller: 'PortafoliocreateCtrl',
            controllerAs: 'vm'
        };
    }

})();
