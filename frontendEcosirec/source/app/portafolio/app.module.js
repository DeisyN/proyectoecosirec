(function () {
    'use strict';

    angular.module('app.portafolio', [
        'app.portafolio.controller',
        'app.portafolio.directivas',
        'app.portafolio.router',
        'app.portafolio.services'
    ]);

})();
