(function () {
    'use strict';
    angular.module('app.portafolio.services', [])
       .factory('Servicios', Servicios);

    Servicios.$inject = ['$resource','BASEURL'];

    function Servicios($resource, BASEURL) {
        return $resource(BASEURL + '/servicios/:idServicio',
        {
          idServicio:'@idServicio'
        },{
          'update': {method: 'PUT'},
          'inhabilitar': {
            url: BASEURL+'/servicios/inhabilitar/:idServicio',
            method: 'PUT',
            //isArray: true,
            params: {idServicio: '@idServicio'}
        }
        });
    }

})();
