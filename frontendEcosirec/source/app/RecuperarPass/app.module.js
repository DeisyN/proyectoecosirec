(function () {
  'use strict';

  angular.module('app.recuperarpass',[
    'app.recuperarpass.services',
    'app.recuperarpass.controller',
    'app.recuperarpass.directivas',
    'app.recuperarpass.router'
  ]);

})();
