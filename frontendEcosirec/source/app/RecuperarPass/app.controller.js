(function () {
  'use strict';

  angular.module('app.recuperarpass.controller',[
  ]).controller('RecuperarContraseñaCtrl', RecuperarContraseñaCtrl)
    .controller('NuevaContraseñaCtrl', NuevaContraseñaCtrl);

  RecuperarContraseñaCtrl.$inject=['RecuperPass', '$mdToast', '$location'];
  function RecuperarContraseñaCtrl(RecuperPass, $mdToast, $location) {
      var vm = this;

      vm.enviar = function(){
        RecuperPass.save(vm.recuperarpass);
        $location.url('/');
        $mdToast.show(
          $mdToast.simple()
          .textContent('Email Enviado...')
          .position('bottom right'))
          .catch(function(error){
            $mdToast.show(
              $mdToast.simple()
              .textContent('Error '+ error +' al Enviar...')
              .position('bottom right'));
          });
      };
  }

  NuevaContraseñaCtrl.$inject=['RecuperPass', '$mdToast', '$location', 'Usuarios', '$stateParams', '$q'];
  function NuevaContraseñaCtrl(RecuperPass, $mdToast, $location, Usuarios, $stateParams, $q) {
      var vm = this;
      vm.usuario = {};

      activate();

      function activate(){
        var promises = [getUsuarios()];
        return $q.all(promises).then(function () {
        });
      }

      function getUsuarios() {
        return Usuarios.findByIdAndCodigo({
          idUsuario: $stateParams.idUsuario, codigoRecuperacionPass: $stateParams.codigoRecuperacionPass
        }).$promise.then(function (data) {
          vm.usuario = data;
        });
      }

      vm.updatepassrec = function(){
          if(vm.password1===vm.password2){
              vm.usuario.password=vm.password1;
                 Usuarios.actualizapass(vm.usuario, function() {
                    $location.path('/');
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Su contraseña ha sido cambiada')
                            .position('bottom right'));
                          },function (error) {
                            $mdToast.show(
                              $mdToast.simple()
                              .textContent('Su contraseña no ha sido cambiada, contraseñas no coinciden' + error)
                              .position('bottom right'));
                            });
          }else{
                   $mdToast.show(
                      $mdToast.simple()
                        .textContent('las contraseñas no coinciden')
                        .position('bottom right'));
          }
        };

    }

})();
