(function () {
  'use strict';

  angular.module('app.recuperarpass.router',[
    'app.recuperarpass.controller'
  ]).config(configure);

      //Se inyecta los parametros
      configure.$inject = ['$stateProvider', '$urlRouterProvider'];

      //Se configura las rutas de la aplicación para modelo
      function configure($stateProvider, $urlRouterProvider) {

          $urlRouterProvider.otherwise('/');

          $stateProvider
              .state('recuperarcontraseña', {
                  url: '/recuperarPassword',
                  template: '<recuperarcontraseña/>'
              }).state('nuevacontraseña', {
                  url: '/nuevaPassword/:idUsuario/:codigoRecuperacionPass',
                  template: '<nuevacontraseña/>'
              });
             }

})();
