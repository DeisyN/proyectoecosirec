(function () {
  'use strict';

  angular.module('app.recuperarpass.services',[

  ]).factory('RecuperPass', RecuperPass);


  RecuperPass.$inject=['$resource', 'BASEURL'];
  function RecuperPass($resource, BASEURL){
    return $resource(BASEURL + '/recuperarPass/recuperarpassword');
  }


})();
