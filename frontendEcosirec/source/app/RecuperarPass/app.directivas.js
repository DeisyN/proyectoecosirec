(function () {
  'use strict';

  angular.module('app.recuperarpass.directivas',[
  ]).directive('recuperarcontraseña', recuperarcontraseña)
    .directive('nuevacontraseña', nuevacontraseña);

  function recuperarcontraseña() {
      return {
          scope: {},
          restrict: 'EA',
          templateUrl: 'app/RecuperarPass/recuperarContraseña.html',
          controller: 'RecuperarContraseñaCtrl',
          controllerAs: 'vm'
      };
  }

  function nuevacontraseña() {
      return {
          scope: {},
          restrict: 'EA',
          templateUrl: 'app/RecuperarPass/nuevaContraseña.html',
          controller: 'NuevaContraseñaCtrl',
          controllerAs: 'vm'
      };
  }

})();
