(function () {
    'use strict';

    angular.module('app.login.directivas', [

    ]).directive('login', login);

    function login() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/login/login.html',
            controller:'LoginCtrl',
            controllerAs:'vm'
        };
    }


    // $(document).ready(function () {
    //
    //   if (this.mostrar == false) {
    //     $('.Header').css({'display':'block'});
    //
    //
    //   }
    //
    //
    // });

})();
