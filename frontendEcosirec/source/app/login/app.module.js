(function(){
  'use strict';
    angular.module('app.login',[
      'app.login.controller',
    	'app.login.router',
      'app.login.directivas',
      'app.config'
    ]);
})();
