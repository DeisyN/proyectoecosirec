(function() {
    'use strict';
    angular.module('app.login.controller', [])
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$auth', '$mdToast', '$location', 'SecurityFilter'];

    function LoginCtrl($auth, $mdToast, $location, SecurityFilter) {
        var vm = this;

        vm.menu = function() {
            if (vm.mostrarMenu == 'MainMenuMostrar') {
                vm.mostrarMenu = '';
            } else {
                vm.mostrarMenu = 'MainMenuMostrar';
            }

        };


        vm.signIn = function() {
            //vm.user = {};
            SecurityFilter.signIn(vm.user);
            //$location.path('/');

        };

        vm.logout = function() {
            SecurityFilter.logout();
            $location.path('/');
        };

        vm.isAuthenticated = function() {
            return SecurityFilter.isAuthenticated();
        };

        vm.isAdmin = function() {
            return SecurityFilter.isAdmin();
        };

        vm.isUser = function() {
            return SecurityFilter.isUser();
        };

        vm.isEmp = function() {
            return SecurityFilter.isEmp();
        };

        vm.getCurrentUser = function() {
            return SecurityFilter.getCurrentUser();
        };

        vm.getCurrentId = function() {
            return SecurityFilter.getCurrentId();
        };

  }
})();
