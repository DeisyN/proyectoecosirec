(function () {
    'use strict';

    angular.module('app.config', [
    ]).constant('BASEURL', 'http://localhost:8080/proyectoSirecBackend/webresources')

    //En este módulo se pueden declarar y asignar todas las constantes http://backendecosirec-ecosirec.rhcloud.com/proyectoSirecBackend/webresources
    //que se usarán en la aplicación. http://localhost:8080/proyectoSirecBackend/webresources
    .config(configure);

    configure.$inject = ['$authProvider', 'BASEURL'];
    function configure($authProvider, BASEURL){
      $authProvider.loginUrl = BASEURL + '/auth/login';
    }

})();
