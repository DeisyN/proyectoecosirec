(function(){
  'use strict';

  angular.module('app.imagenes', [])
  .directive('onReadImg', onReadImg);

  /*@ngInject */
  function onReadImg($parse) {
    var directive = {
      link: link,
      restrict: 'A',
      scope: false
    };
    return directive;

    function link(scope, element, attrs) {
      var fn = $parse(attrs.onReadImg);
      element.on('change', function(onChangeEvent) {
        var reader = new FileReader();
        reader.onload = function(onLoadEvent) {
          console.log('VALIDACIÓN TAMAÑO IMAGEN');

          if(onLoadEvent.total <= 800000){
            console.log('OK');
            scope.$apply(function() {
              fn(scope, {
                //leer imagen y quitar la primera parte de data:image/png;base64, ....
                $fileContent: onLoadEvent.target.result.split(',')[1]
              });
            });
          } else {
            console.log('La imágen es muy grande');
            scope.$apply(function() {
              fn(scope, {

                  //leer imagen y quitar la primera parte de data:image/png;base64, ....
                  $fileContent: 1
          });
          });
          }


        };
        reader.readAsDataURL((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
      });
    }
  }
})();
