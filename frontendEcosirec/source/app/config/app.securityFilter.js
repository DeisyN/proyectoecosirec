(function () {
    'use strict';

    angular.module('app.securityFilter', [
      'satellizer'
    ])
      .factory('SecurityFilter', SecurityFilter);

      SecurityFilter.$inject = ['$auth', '$mdToast', '$location'];
      function  SecurityFilter ($auth, $mdToast, $location){
        var Filter =  {
            isAuthenticated:function(){
                if($auth.isAuthenticated()){
                  return true;
                }else{
                  return false;
                }
            },
            isAdmin:function(){
              if(Filter.isAuthenticated()){
                return $auth.getPayload().roles.indexOf('ADMIN') !== -1; // -1 si ADMIN no esta en el arreglo y retorna false
              }else{
                return false;
              }
            },
            isUser:function(){
                if(Filter.isAuthenticated()){
                  return $auth.getPayload().roles.indexOf('USER') !== -1; // -1 si ADMIN no esta en el arreglo y retorna false
                }else{
                  return false;
                }
              },
            isEmp:function (){
              if(Filter.isAuthenticated()){
                return $auth.getPayload().roles.indexOf('EMP') !== -1; // -1 si es empresario no esta en el arreglo y retorna false
              }else{
                return false;
              }
            },
             getCurrentUser:function(){
                if(Filter.isAuthenticated()){
                  return $auth.getPayload().user;
                }else{
                  return;
                }
              },
             getCurrentId:function (){
                if(Filter.isAuthenticated()){
                  return $auth.getPayload().sub; //.id
                }else{
                  return;
                }
              },
              isEstado:function(){
                  return $auth.isEstado();
              },
              signIn:function(dataLogin){
                 $auth.login(dataLogin)
                  .then(function(){
                    $location.path('/');
                    $mdToast.show(
                      $mdToast.simple()
                      .textContent('Sesion iniciada correctamente...')
                      .position('bottom right'));

                  })
                  .catch(function(err){
                    console.log(err.status+' '+err.data);
                    $mdToast.show(
                      $mdToast.simple()
                      .textContent('Error de email o de password...')
                      .position('bottom right'));
                  });
              },
              logout:function(){
                if(Filter.isAuthenticated()){
                    $auth.logout()
                    .then(function(){
                      $location.path('/');
                      $mdToast.show(
                          $mdToast.simple()
                            .textContent('Sesion finalizada correctamente...')
                            .position('bottom right'));
                      console.log('Sesion finalizada correctamente...');
                  });

                  }else{
                    return;
                  }
              }

          };

          return Filter;

      }//funcion
})();
