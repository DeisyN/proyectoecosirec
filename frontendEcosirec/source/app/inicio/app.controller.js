(function() {
    angular.module('app.inicio.controller', [])
        .controller('InicioCtrl', InicioCtrl);

    InicioCtrl.$inject = ['$location'];
    function InicioCtrl($location) {



      $('a.ancla').click(function(e){
          e.preventDefault();
            enlace  = $(this).attr('href');
            $('html, body').animate({
              scrollTop: $(enlace).offset().top
              }, 1000);
        });


      //vamos al principio o al final de la página
      $('a.arriba').click(function(e){
      e.preventDefault();
          volver  = $(this).attr('href');
          $('html, body').animate({
              scrollTop: $(volver).offset().top
          }, 2000);
      });

      $("a.abajo").click(function () {
          $('html,body').animate({
            scrollTop: $("#Ambiental").offset().top - 90
          }, 2000);
      });



        $(document).ready(function() {


            if ($location.path() === '/') {

                console.log("es inicio");
                onscroll = function() {
                    var menuTransparencia = $(document).scrollTop();
                    if (menuTransparencia <= 50) {

                        $('#Header').css({
                            'background': 'transparent'
                        });
                        $('.iconomenu').css({
                            'display': 'none'
                        });
                        $('#MainMenuInicio2').css({
                            'display': 'none'
                        });
                        $('#MainMenuInicio').css({
                            'display': 'flex'
                        });
                        $('.iconomenu2').css({
                            'display': 'inline-block'
                        });
                        //$('#MainMenuInicio').removeClass( "MainMenuMostrar");
                    } else {
                        $('.iconomenu2').css({
                            'display': 'none'
                        });
                        $('#MainMenuInicio').css({
                            'display': 'none'
                        });

                        $('#MainMenuInicio2').css({
                            'display': 'flex'
                        });
                        $('#Header').css({
                            'background': '#3c948b'
                        });
                        $('.iconomenu').css({
                            'display': 'block',
                            'margin-top': '.1em',
                            'margin-left': ' -.3em'
                        });

                    }
                };




                /**SLIDER**/

                var slider = $('#slider');
                var siguiente = $('#btn-next');
                var anterior = $('#btn-prev');

                //mover ultima imagen al primer lugar
                $('#slider .slider__section:last').insertBefore('#slider .slider__section:first');
                //mostrar la primera imagen con un margen de -100%
                slider.css('margin-left', '-' + 100 + '%');

                function moverDerecha() {
                    slider.animate({
                        marginLeft: '-' + 200 + '%'
                    }, 700, function() {
                        $('#slider .slider__section:first').insertAfter('#slider .slider__section:last');
                        slider.css('margin-left', '-' + 100 + '%');
                    });
                }

                function moverIzquierda() {
                    slider.animate({
                        marginLeft: 0
                    }, 700, function() {
                        $('#slider .slider__section:last').insertBefore('#slider .slider__section:first');
                        slider.css('margin-left', '-' + 100 + '%');
                    });
                }
                //hacer que el slider sea automático
                function autoplay() {
                    interval = setInterval(function() {
                        moverDerecha();
                    }, 5000);
                }

                siguiente.on('click', function() {
                    moverDerecha();
                    clearInterval(interval);
                    autoplay();
                });

                anterior.on('click', function() {
                    moverIzquierda();
                    clearInterval(interval);
                    autoplay();
                });

                      autoplay();
                /** FIN SLIDER **/
                /**SLIDER**/

                var slider2 = $('#slider2');
                var siguiente = $('#btn-next1');
                var anterior = $('#btn-prev1');

                //mover ultima imagen al primer lugar
                $('#slider2 .slider2__section:last').insertBefore('#slider2 .slider2__section:first');
                //mostrar la primera imagen con un margen de -100%
                slider2.css('margin-left', '-' + 100 + '%');

                function moverDerec() {
                    slider2.animate({
                        marginLeft: '-' + 200 + '%'
                    }, 700, function() {
                        $('#slider2 .slider2__section:first').insertAfter('#slider2 .slider2__section:last');
                        slider2.css('margin-left', '-' + 100 + '%');
                    });
                }

                function moverIzquier() {
                    slider2.animate({
                        marginLeft: 0
                    }, 700, function() {
                        $('#slider2 .slider2__section:last').insertBefore('#slider2 .slider2__section:first');
                        slider2.css('margin-left', '-' + 100 + '%');
                    });
                }
                //hacer que el slider sea automático
                function autoplay1() {
                    interval = setInterval(function() {
                        moverDerec();
                    }, 5000);
                }

                siguiente.on('click', function() {
                    moverDerec();
                    clearInterval(interval);
                    autoplay1();
                });

                anterior.on('click', function() {
                    moverIzquier();
                    clearInterval(interval);
                    autoplay1();
                });

                autoplay1();

                /** FIN SLIDER **/

            } else {
                console.log("no es inicio");

                $('.iconomenu2').css({
                    'display': 'none'
                });
                $('#MainMenuInicio').css({
                    'display': 'none'
                });
                $('#MainMenuInicio2').css({
                    'display': 'none'
                });
                $('#MainMenu2').css({
                    'display': 'flex!important'
                });
            }


        });




    }

})();
