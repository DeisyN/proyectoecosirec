(function () {
    'use strict';

    angular.module('app.papelera.router', [
        'app.papelera.controller'
    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('papelera', {
                url: '/papelera',
                template: '<papelera></papelera>'
            });
    }
})();
