(function () {
    'use strict';

    angular.module('app.papelera', [
        'app.papelera.controller',
        'app.papelera.directivas',
        'app.papelera.router'

    ]);

})();
