(function () {
    'use strict';

    angular.module('app.papelera.directivas', [

    ]).directive('papelera', papelera);

  function papelera(){
    return{
      scope: {},
      restrict: 'EA',
      templateUrl: 'app/papelera/papelera.html',
      controller: 'papeleraCtrl',
      controllerAs: 'vm'
    };
  }

})();
