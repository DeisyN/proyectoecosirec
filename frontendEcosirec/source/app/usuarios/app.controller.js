(function() {
    'use strict';

    angular.module('app.usuarios.controller', [

        ]).controller('UsuariosCtrl', UsuariosCtrl)
        .controller('UsuariosEmpCtrl', UsuariosEmpCtrl)
        .controller('UsuarioscreateCtrl', UsuarioscreateCtrl)
        .controller('UsuariosPerfilCtrl', UsuariosPerfilCtrl)
        .controller('UsuariosUpdateCtrl', UsuariosUpdateCtrl)
        .controller('UsuariosemailCtrl', UsuariosemailCtrl)
        .controller('UpdatepassCtrl', UpdatepassCtrl)
        .controller('ListempviewCrtl', ListempviewCrtl)
        .controller('ListCalificacionesCtrl', ListCalificacionesCtrl)
        .controller('CalificarCtrl', CalificarCtrl)
        .controller('CalificacionesViewCrtl', CalificacionesViewCrtl)
        .controller('InhabilitarCuentaCtrl', InhabilitarCuentaCtrl);

    UsuariosCtrl.$inject = ['Usuarios', '$mdToast', '$location', 'SecurityFilter'];

    function UsuariosCtrl(Usuarios, $mdToast, $location, SecurityFilter) {
        var vm = this;
        vm.query = {
            order: 'name',
            limit: 5,
            page: 1

        };

        if (!SecurityFilter.isAdmin()) {
            $location.path('/');
        } else {
            vm.usuarios = Usuarios.query().$promise.then(function(data) {
                vm.usuarios = data;
            });
        }

        vm.inhabilitar = function(id) {
            Usuarios.inhabilitar({
                    idUsuario: id
                }).$promise
                .then(function(data) {
                    console.log("realizado con exito");
                    console.log(data);
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Estado actualizado exitosamente...')
                        .position('bottom right'));
                })
                .catch(function(data) {
                    console.log("fallo en inhabilitar");
                    console.log(data);
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Error al actualizar el estado...')
                        .position('bottom right'));
                });
        };
    }

    UsuariosEmpCtrl.$inject = ['Usuarios', '$location', 'SecurityFilter'];

    function UsuariosEmpCtrl(Usuarios, $location, SecurityFilter) {
        var vm = this;
        vm.query = {
            order: 'name',
            limit: 5,
            page: 1

        };

        if (SecurityFilter.isUser() || SecurityFilter.isEmp()) {
            vm.empresas = Usuarios.searchEmpresas();

        } else {
            $location.path('/');
        }
    }


    ListCalificacionesCtrl.$inject = ['Calificaciones', '$location', 'SecurityFilter'];

    function ListCalificacionesCtrl(Calificaciones, $location, SecurityFilter) {
        var vm = this;
        vm.query = {
            order: 'name',
            limit: 5,
            page: 1

        };

        if (SecurityFilter.isUser()) {
            vm.calificaciones = Calificaciones.query();
            console.log(vm.calificaciones);
        } else {
            $location.path('/');
        }

    }

    CalificarCtrl.$inject = ['Calificaciones', 'Usuarios', '$location', '$mdToast', '$stateParams', 'SecurityFilter'];

    function CalificarCtrl(Calificaciones, Usuarios, $location, $mdToast, $stateParams, SecurityFilter) {
        var vm = this;

        if (SecurityFilter.isUser()) {
            vm.usuario = Usuarios.get({
                idUsuario: $stateParams.idUsuario
            });
            vm.calificar = function(idEmpresa) {
                vm.calificaciones.idEmpresa = {
                    idUsuario: idEmpresa
                };

                Calificaciones.save(vm.calificaciones, function(Data) {
                    console.log(Data);
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Calificacion exitosa...')
                        .position('bottom right'));
                    $location.path('/usuarios/empresas');
                });
            };
        } else {
            $location.path('/');
        }
    }

    ListempviewCrtl.$inject = ['Usuarios', '$location', '$stateParams', 'SecurityFilter'];

    function ListempviewCrtl(Usuarios, $location, $stateParams, SecurityFilter) {
        var vm = this;
        if (SecurityFilter.isUser() || SecurityFilter.isEmp()) {
            vm.usuario = Usuarios.get({
                idUsuario: $stateParams.idUsuario
            });
        } else {
            $location.path('/');
        }
    }

    CalificacionesViewCrtl.$inject = ['Usuarios', '$stateParams', 'Calificaciones', '$location', 'SecurityFilter'];

    function CalificacionesViewCrtl(Usuarios, $stateParams, Calificaciones, $location, SecurityFilter) {
        /*this.usuario = Usuarios.get({
            idUsuario: $stateParams.idUsuario
        });
        */
        var vm = this;

        vm.query = {
            order: 'name',
            limit: 5,
            page: 1

        };

        if (SecurityFilter.isUser()) {
            vm.total = 0;
            vm.nombre = ' ';
            Calificaciones.searchCalificacionesByIdEmpresa({
                    idEmpresa: $stateParams.idEmpresa
                }).$promise
                .then(function(data) {
                    vm.calificaciones = data;
                    vm.nombre = vm.calificaciones[0].idEmpresa.nombre;

                    for (var i = 0; i < vm.calificaciones.length; i++) {

                        vm.total = vm.total + vm.calificaciones[i].calificacion;
                    }
                    vm.totalV = vm.total/vm.calificaciones.length;
                })
                .catch(function(err) {
                    console.log(err);
                });
        } else {
            $location.path('/');
        }
    }


    UsuarioscreateCtrl.$inject = ['Usuarios', 'Ciudades', 'TiposDocumento', 'TiposUsuarios', '$location', '$mdToast', '$state'];

    function UsuarioscreateCtrl(Usuarios, Ciudades, TiposDocumento, TiposUsuarios, $location, $mdToast, $state) {
        //  this.ciudades = Ciudades.query();
        var vm = this;
        vm.queryCiudades = queryCiudades;
        // vm.tiposDocumento = TiposDocumento.query();
        // vm.tipoUsuarios = TiposUsuarios.query();
        vm.dateMax = new Date();
        vm.dateMax.setFullYear(vm.dateMax.getFullYear() - 18);
        // vm.loadImg = loadImg;
        vm.validaEmail = false;
        vm.validaNum = false;
        vm.errorValida = "";
        vm.usuario = {};
        vm.mens = '';
        vm.img = false;

        vm.validaCorreo = function(){
          if(vm.usuario.email) {
            Usuarios.findByEmail({email:vm.usuario.email}).$promise.then(function(data){
              vm.validaEmail = false;
            }).catch(function(err){
              vm.validaEmail = true;
              $mdToast.show(
                $mdToast.simple()
                .textContent('El Email ya se encuentra registrado...')
                .position('bottom right'));
            });
          } else {
            vm.validaEmail = false;
          }
        };

        vm.validaNumIdent = function(){
          if(vm.usuario.razonSocial) {
            Usuarios.findByNumIdent({razonSocial:vm.usuario.razonSocial}).$promise.then(function(data){
              vm.validaNum = false;
            }).catch(function(err){
              vm.validaNum = true;
              $mdToast.show(
                $mdToast.simple()
                .textContent('El Número de identificación ya se encuentra registrado...')
                .position('bottom right'));
            });
          } else {
            vm.validaNum = false;
          }
        };

        console.log(vm.queryCiudades);

        vm.create = function() {
          if(!vm.validaEmail || !vm.validaNum){
            console.log(vm.usuario);
            Usuarios.save(vm.usuario).$promise.then(function(data){
              // $location.path('/');
              $mdToast.show(
                $mdToast.simple()
                .textContent('Se ha  Registrado satisfactoriamente...')
                .position('bottom right'));
                vm.usuario = {};
                $state.go('login');
            }).catch(function(){
              $mdToast.show(
                $mdToast.simple()
                .textContent('Error al crear usuario...')
                .position('bottom right'));
            });
          } else {
            vm.errorValida = "Ingrese todos los campos en rojo...";
            $mdToast.show(
              $mdToast.simple()
              .textContent('Ingrese todos los campos en rojo...')
              .position('bottom right'));
          }
            // console.log(vm.usuario);
            // Usuarios.save(vm.usuario, function() {
            //     $location.path('/');
            //     $mdToast.show(
            //         $mdToast.simple()
            //         .textContent('Se ha  Registrado satisfactoriamente...')
            //         .position('bottom right'));
            // });
        };

        vm.loadImg = function($fileContent) {
          vm.mens = '';
          vm.img = false;
          console.log($fileContent.length);

          if($fileContent.length > 2000){
            console.log('HOLA DE TRUE');
              vm.usuario.foto = $fileContent;
          } else {
            console.log('HOLA DE FALSE');
            vm.mens = 'La imágen es muy grande!';
            vm.img = true;
            vm.usuario.foto = null;
          }
        };

        vm.mensaje = function(){
          return vm.mens;
        };

        vm.imagen = function(){
          return vm.img;
        };

        function queryCiudades(str) {
            return Ciudades.searchCiudadesByNombre({
                query: str
            }).$promise;
        }
    }

    UsuariosemailCtrl.$inject = ['Usuarios', 'Email', '$stateParams', '$location', '$mdToast', 'SecurityFilter'];

    function UsuariosemailCtrl(Usuarios, Email, $stateParams, $location, $mdToast, SecurityFilter) {
        var vm = this;
        if (SecurityFilter.isUser()) {
            vm.id = $stateParams.idUsuario;
            Usuarios.get({
                idUsuario: vm.id
            }).$promise.then(function(data) {
                vm.email = {};
                vm.email.emailEmpresa = data.email;
            });
            vm.enviar = function() {
                Email.save(vm.email, function() {
                    console.log('MENSAJE: ' + vm.email);
                    $location.path('/usuarios/empresas');
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Su mensaje ha sido enviado exitosamente...')
                        .position('bottom right'));
                });
            };
        } else {
            $location.path('/');
        }
    }


    UsuariosPerfilCtrl.$inject = ['Usuarios', '$location', 'SecurityFilter'];

    function UsuariosPerfilCtrl(Usuarios, $location, SecurityFilter) {
        var vm = this;

        if (SecurityFilter.isAuthenticated()) {
            vm.usuario = Usuarios.get({
                idUsuario: SecurityFilter.getCurrentId()
            });
        } else {
            $location.path('/');
        }

    }

    UsuariosUpdateCtrl.$inject = ['Usuarios', '$location', '$mdToast', 'Ciudades', 'SecurityFilter'];

    function UsuariosUpdateCtrl(Usuarios, $location, $mdToast, Ciudades, SecurityFilter) {
        //this.ciudades = Ciudades.query();
        var vm = this;
        vm.mens = '';
        vm.img = false;
        vm.validaEmail = false;

        if (SecurityFilter.isAuthenticated()) {
            // vm.queryCiudades = queryCiudades;
            vm.usuario = Usuarios.get({
                idUsuario: SecurityFilter.getCurrentId()
            });

            // vm.loadImg = loadImg;

            vm.validaCorreo = function(){
              if(vm.usuario.email) {
                Usuarios.findByEmail({email:vm.usuario.email}).$promise.then(function(data){
                  vm.validaEmail = false;
                }).catch(function(err){
                  vm.validaEmail = true;
                  $mdToast.show(
                    $mdToast.simple()
                    .textContent('El Email ya se encuentra registrado...')
                    .position('bottom right'));
                });
              } else {
                vm.validaEmail = false;
              }
            };

            vm.update = function() {
              if(!vm.validaEmail){
                console.log(vm.usuario);
                Usuarios.update(vm.usuario).$promise.then(function(data){
                    $location.path('/usuarios/perfil');
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Se ha  actualizado el  Usuario...')
                        .position('bottom right'));
                }).catch(function(){
                  $location.path('/usuarios/perfil');
                  $mdToast.show(
                    $mdToast.simple()
                    .textContent('Error al actualizar usuario...')
                    .position('bottom right'));
                });
              } else {
                $mdToast.show(
                  $mdToast.simple()
                  .textContent('Ingrese todos los campos en rojo...')
                  .position('bottom right'));
              }
            };

             vm.loadImg = function($fileContent) {
               vm.mens = '';
               vm.img = false;
               console.log($fileContent.length);

               if($fileContent.length > 2000){
                 console.log('HOLA DE TRUE');
                   vm.usuario.foto = $fileContent;
               } else {
                 console.log('HOLA DE FALSE');
                 vm.mens = 'La imágen es muy grande!';
                 vm.img = true;
                 vm.usuario.foto = null;
               }
             };

               vm.mensaje = function(){
               return vm.mens;
               };

               vm.imagen = function(){
               return vm.img;
               };

            vm.queryCiudades = function(str) {
                return Ciudades.searchCiudadesByNombre({
                    query: str
                }).$promise;
            };

        } else {
            $location.path('/');
        }
    }

    UpdatepassCtrl.$inject = ['Usuarios', '$location', '$mdToast', 'SecurityFilter', '$state'];

    function UpdatepassCtrl(Usuarios, $location, $mdToast, SecurityFilter, $state) {
        var vm = this;
        // vm.confirmar = {};
        if (SecurityFilter.isAuthenticated()) {
            vm.updatepass = function() {
                //vm.usuario.password = vm.passwordnew;
                Usuarios.updatePassword({
                    id: SecurityFilter.getCurrentId(),
                    passOld: vm.passwordNow,
                    passNew: vm.passwordNew
                }).$promise.then(function(data){
                  console.log('OK');
                  console.log(data);
                  $location.path('/usuarios/perfil');
                  $mdToast.show(
                      $mdToast.simple()
                      .textContent('Se ha  actualizado la contraseña correctamente...')
                      .position('bottom right'));
                }).catch(function(err){
                  console.log('ERROR');
                  console.log(err);
                  $location.path('/usuarios/perfil');
                  $mdToast.show(
                      $mdToast.simple()
                      .textContent('Error al actualizar, las contraseña actual no coincide...' + err)
                      .position('bottom right'));
                });

            };
        } else {
            $location.path('/');
        }
    }

    InhabilitarCuentaCtrl.$inject = ['Usuarios', '$location', '$mdToast', 'SecurityFilter'];

    function InhabilitarCuentaCtrl(Usuarios, $location, $mdToast, SecurityFilter) {
        var vm = this;
        if (SecurityFilter.isAuthenticated()) {
            vm.logout = function() {
                SecurityFilter.logout();
                $location.path('/');
            };

            vm.getCurrentUser = function() {
                return SecurityFilter.getCurrentUser();
            };
//confirmar
            vm.confirmar = function() {
                //vm.usuario.password = vm.passwordnew;
                Usuarios.confirmarPassword({
                    id: SecurityFilter.getCurrentId(),
                    passOld: vm.passwordNow
                }).$promise.then(function(data){
                  console.log('OK');
                  console.log(data);
                  $location.path('/usuarios/perfil/desactivarcuenta');
                  $mdToast.show(
                      $mdToast.simple()
                      .textContent('Contraseña correcta...')
                      .position('bottom right'));
                }).catch(function(err){
                  console.log('ERROR');
                  console.log(err);
                  $location.path('/usuarios/perfil');
                  $mdToast.show(
                      $mdToast.simple()
                      .textContent('Contraseña incorrecta...' + err)
                      .position('bottom right'));
                });

            };
//---fin confirmar
            vm.inhabilitar = function() {
                Usuarios.inhabilitar({
                        idUsuario: SecurityFilter.getCurrentId()
                    }).$promise
                    .then(function(data) {
                        $mdToast.show(
                            $mdToast.simple()
                            .textContent('Gracias por todo')
                            .position('bottom right'));
                        vm.logout();
                        // SecurityFilter.logout();
                        $location.path('/');
                    });
            };

        } else {
            $location.path('/');
        }

    }


    getCiudades.$inject = ['Ciudades'];

    function getCiudades(Ciudades) {
        return Ciudades.query();
    }

    getRoles.$inject = ['Roles'];

    function getRoles(Roles) {
        return Roles.query();
    }

})();
