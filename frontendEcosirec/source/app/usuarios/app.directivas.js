(function () {
    'use strict';

    angular.module('app.usuarios.directivas', [

        ]).directive('usuarios', usuarios)
        .directive('create', create)
        .directive('perfil', perfil)
        .directive('usuariosupdate', usuariosUpdate)
        .directive('usuariosemp', usuariosemp)
        .directive('usuariosemail', usuariosemail)
        .directive('updatepass', updatepass)
        .directive('listempview', listempview)
        .directive('calificacionslist',calificacionesList)
        .directive('calificar', calificar)
        .directive('calificacionesview', calificacionesView)
        .directive('desactivarcuenta', desactivarcuenta)
        .directive('confirmarpassd', confirmarpassd);

    function calificacionesView(){
      return {
          scope: {},
          restrict: 'EA',
          templateUrl: 'app/usuarios/calificacionesview.html',
          controller: 'CalificacionesViewCrtl',
          controllerAs: 'vm'

      };

    }

    function calificacionesList(){
      return {
          scope: {},
          restrict: 'EA',
          templateUrl: 'app/usuarios/calificacioneslist.html',
          controller: 'ListCalificacionesCtrl',
          controllerAs: 'vm'
      };

    }

    function calificar(){
      return {
          scope: {},
          restrict: 'EA',
          templateUrl: 'app/usuarios/calificar.html',
          controller: 'CalificarCtrl',
          controllerAs: 'vm'
      };

    }

    function usuarios() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/usuarios/lista.html',
            controller: 'UsuariosCtrl',
            controllerAs: 'vm'
        };
    }

    function updatepass() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/usuarios/updatepass.html',
            controller: 'UpdatepassCtrl',
            controllerAs: 'vm'
        };
    }

    function usuariosemp() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/usuarios/listaEmpresas.html',
            controller: 'UsuariosEmpCtrl',
            controllerAs: 'vm'
        };
    }

    function create() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/usuarios/create.html',
            controller: 'UsuarioscreateCtrl',
            controllerAs: 'vm'
        };
    }

    function perfil() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/usuarios/perfil.html',
            controller: 'UsuariosPerfilCtrl',
            controllerAs: 'vm'
        };
    }

    function usuariosUpdate() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/usuarios/editar.html',
            controller: 'UsuariosUpdateCtrl',
            controllerAs: 'vm'
        };
    }

    function usuariosemail() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/usuarios/contactar.html',
            controller: 'UsuariosemailCtrl',
            controllerAs: 'vm'
        };
    }

    function listempview() {
      return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/usuarios/listaempview.html',
            controller: 'ListempviewCrtl',
            controllerAs: 'vm'
        };
    }

    function desactivarcuenta(){
      return {
          scope: {},
          restrict: 'EA',
          templateUrl: 'app/usuarios/inhabilitarcuenta.html',
          controller: 'InhabilitarCuentaCtrl',
          controllerAs: 'vm'

      };
    }

    function confirmarpassd(){
      return {
          scope: {},
          restrict: 'EA',
          templateUrl: 'app/usuarios/confirmarinhabilit.html',
          controller: 'InhabilitarCuentaCtrl',
          controllerAs: 'vm'

        };
    }

})();
