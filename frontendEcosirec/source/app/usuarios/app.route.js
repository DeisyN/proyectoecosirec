(function() {
    'use strict';

    angular.module('app.usuarios.router', [
        'app.usuarios.services',
        'app.usuarios.controller'
    ]).config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('usuarios', {
                url: '/usuarios',
                template: '<usuarios></usuarios>'
            }).state('usuariosemp', {
                url: '/usuarios/empresas',
                template: '<usuariosemp></usuariosemp>'
            }).state('create', {
                url: '/usuarios/create',
                template: '<create></create>'
            }).state('perfil', {
                url: '/usuarios/perfil',
                template: '<perfil></perfil>'
            }).state('usuariosupdate', {
                url: '/usuarios/update',
                template: '<usuariosupdate></usuariosupdate>'
            }).state('usuariosemail', {
                url: '/usuarios/contactar/:idUsuario',
                template: '<usuariosemail></usuariosemail>'
            }).state('updatepass', {
                url: '/usuarios/updatepass',
                template: '<updatepass></updatepass>'
            }).state('listempview', {
                url: '/usuarios/listempview/:idUsuario',
                template: '<listempview></listempview>'
            }).state('calificacioneslist', {
                url: '/usuarios/calificaciones',
                template: '<calificacionslist></calificacionslist>'
            }).state('calificar', {
                url: '/usuarios/calificar/:idUsuario',
                template: '<calificar></calificar>'
            }).state('calificacionesview', {
                url: '/usuarios/calificaciones/:idEmpresa',
                template: '<calificacionesview></calificacionesview>'
            }).state('desactivarcuenta', {
                url: '/usuarios/perfil/desactivarcuenta',
                template: '<desactivarcuenta></desactivarcuenta>'
            }).state('confirmarpassd', {
                url: '/usuarios/perfil/confirmarpasswd',
                template: '<confirmarpassd></confirmarpassd>'
            });
    }
})();
