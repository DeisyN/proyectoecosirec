(function () {
    'use strict';
    angular.module('app.usuarios.services', [])
       .factory('Usuarios', Usuarios)
       .factory('Ciudades', Ciudades)
       .factory('Email', Email)
       .factory('TiposDocumento', TiposDocumento)
       .factory('TiposUsuarios', TiposUsuarios)
       .factory('Calificaciones', Calificaciones);

    Usuarios.$inject = ['$resource','BASEURL'];

    //Este servicio nos provee de los métodos GET POST PUT DELETE
    function Usuarios($resource, BASEURL) {
        return $resource(BASEURL + '/usuarios/:idUsuario',
        //Se debe digitar el id del modelo
        //{ inmuebleId: '@_id' },
        {
          idUsuario:'@idUsuario'
        },{
          'update': {method: 'PUT'},                   //'get':  {method:'GET'},'save':   {method:'POST'},'query':  {method:'GET', isArray:true},'delete': {method:'DELETE'},
          'searchEmpresas':{
            url: BASEURL + '/usuarios/empresas',
            method: 'GET',
            isArray: true,
          },
          'searchEmpresasid':{
            url: BASEURL + '/usuarios/:idUsuario',
            method: 'GET',
            isArray: true,
          },
          updatePassword: {
            url: BASEURL + '/usuarios/updatepass/:id/:passOld/:passNew',
            method: 'PUT',
            params: {
              id: '@id',
              passOld: '@passOld',
              passNew: '@passNew'
            }
          },
          confirmarPassword: {
            url: BASEURL + '/usuarios/confirmarPassword/:id/:passOld',
            method: 'POST',
            params: {
              id: '@id',
              passOld: '@passOld'
            }
          },
          inhabilitar: {
            url: BASEURL+'/usuarios/inhabilitar/:idUsuario',
            method: 'PUT',
            params: {idUsuario: '@idUsuario'}
          },
          findByEmail: {
            url: BASEURL + '/usuarios/findByEmail/:email',
            method: 'GET',
            params: {
              email: '@email'
            }
          },
          findByNumIdent: {
            url: BASEURL + '/usuarios/findByNumIdent/:razonSocial',
            method: 'GET',
            params: {
              razonSocial: '@razonSocial'
            }
          },
          findByIdAndCodigo:{
                url: BASEURL + '/usuarios/codigoRecuperacion/:idUsuario/:codigoRecuperacionPass',
                method: 'GET',
                params:{idUsuario: '@idUsuario',
                    codigoRecuperacionPass: '@codigoRecuperacionPass'}

          },
          actualizapass:{
            url: BASEURL + '/usuarios/actualizapass/:idUsuario',
            method: 'PUT',
            params: {idUsuario: '@idUsuario'}
          }
        });
    }

    Email.$inject = ['$resource', 'BASEURL'];

    function Email($resource, BASEURL) {
        return $resource(BASEURL + '/contactar');
      //{ idUsuario: '@idUsuario'},
      //  { 'save': {method: 'POST'}});
    }

    Ciudades.$inject = ['$resource', 'BASEURL'];

    function Ciudades($resource, BASEURL) {
        return $resource(BASEURL + '/ciudades/:idCiudad',
        {
          idCiudad: '@idCiudad'
        },
        {
          searchCiudadesByNombre:{
            url: BASEURL + '/ciudades/nombre/:query' ,
            method: 'GET',
            isArray: true,
            params:{
              query:'@query'
            }
          }
        }/*,
        { 'get':    {method:'GET'},
          'query':  {method:'GET', isArray:true},

        }*/);
    }

    TiposDocumento.$inject = ['$resource', 'BASEURL'];

    function TiposDocumento($resource, BASEURL) {
        return $resource(BASEURL + '/tiposDocumento');
      //{ idUsuario: '@idUsuario'},
      //  { 'save': {method: 'POST'}});
    }

    TiposUsuarios.$inject = ['$resource', 'BASEURL'];

    function TiposUsuarios($resource, BASEURL) {
        return $resource(BASEURL + '/tipoUsuarios');
      //{ idUsuario: '@idUsuario'},
      //  { 'save': {method: 'POST'}});
    }


    Calificaciones.$inject = ['$resource', 'BASEURL'];

    function Calificaciones($resource, BASEURL) {
        return $resource(BASEURL + '/calificaciones/:idCalificacion',

        {
          idCalificacion: '@idCalificacion'
        },
        {
          searchCalificacionesByIdEmpresa:{
            url: BASEURL + '/calificaciones/total/:idEmpresa' ,
            method: 'GET',
            isArray: true,
            params:{
              idEmpresa:'@idEmpresa'
            }
          }
        }
        );
    }

    //De igual manera los factory nos sirven para almacenar información
    //y que nos pueda servir en cualquier controlador o lugar de la aplicación
    //evitando de esta manera hacer variables globales.*/
})();
