(function () {
    'use strict';

    angular.module('app.usuarios', [
       'app.usuarios.controller',
       'app.usuarios.directivas',
       'app.usuarios.router',
       'app.usuarios.services'

    ]);

})();
