(function() {
    'use strict';

    angular.module('app.anuncios.router', [

    ]).config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('anuncios', {
                url: '/anuncios',
                template: '<anuncios></anuncios>'
            }).state('misanuncios', {
                  url: '/misanuncios',
                  template: '<misanuncios></misanuncios>'
              }).state('createanuncios', {
                url: '/anuncios/create',
                template: '<createanuncios></createanuncios>'
            }).state('updateanuncios', {
                url: '/anuncios/editar/:idAnuncio',
                template: '<updateanuncios></updateanuncios>'
            });

    }
})();
