(function () {
    'use strict';

    angular.module('app.anuncios', [
       'app.anuncios.controller',
       'app.anuncios.directivas',
       'app.anuncios.router',
       'app.anuncios.services'

    ]);

})();
