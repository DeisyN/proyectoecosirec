(function() {
    'use strict';

    angular.module('app.anuncios.controller', [

        ]).controller('AnunciosCtrl', AnunciosCtrl)
        .controller('AnuncioscreateCtrl', AnuncioscreateCtrl)
        .controller('AnunciosUpdateCtrl', AnunciosUpdateCtrl)
        .controller('MisAnunciosCtrl', MisAnunciosCtrl);

    AnunciosCtrl.$inject = ['Anuncios', '$mdToast', 'SecurityFilter', '$location'];

    function AnunciosCtrl(Anuncios, $mdToast, SecurityFilter, $location) {
        var vm = this;

        vm.query = {
            order: 'name',
            limit: 5,
            page: 1

        };

        if (SecurityFilter.isUser() || SecurityFilter.isAdmin()) {

            vm.anuncios = Anuncios.query().$promise
                .then(function(data) {
                    vm.anuncios = data;
                });

            vm.inhabilitar = function(id) {

                Anuncios.inhabilitar({
                        idAnuncio: id
                    }).$promise
                    .then(function(data) {
                        console.log("realizado con exito");
                        console.log(data);
                        $mdToast.show(
                            $mdToast.simple()
                            .textContent('Estado actualizado exitosamente...')
                            .position('bottom right'));
                    })
                    .catch(function(data) {
                        console.log("fallo en inhabilitar");
                        console.log(data);
                        $mdToast.show(
                            $mdToast.simple()
                            .textContent('Error al actualizar el estado...')
                            .position('bottom right'));
                    });
            };

        } else {
            $location.path('/');
        }
    }

    MisAnunciosCtrl.$inject = ['Anuncios', '$mdToast', 'SecurityFilter', '$location'];

    function MisAnunciosCtrl(Anuncios, $mdToast, SecurityFilter, $location) {
        var vm = this;

        vm.query = {
            order: 'name',
            limit: 5,
            page: 1

        };

        if (SecurityFilter.isEmp() || SecurityFilter.isAdmin()) {
            vm.anunciosId = Anuncios.searchIdUsuario();

            vm.inhabilitar = function(id) {

                Anuncios.inhabilitar({
                        idAnuncio: id
                    }).$promise
                    .then(function(data) {
                        console.log("realizado con exito");
                        console.log(data);
                        $mdToast.show(
                            $mdToast.simple()
                            .textContent('Estado actualizado exitosamente...')
                            .position('bottom right'));
                    })
                    .catch(function(data) {
                        console.log("fallo en inhabilitar");
                        console.log(data);
                        $mdToast.show(
                            $mdToast.simple()
                            .textContent('Error al actualizar el estado...')
                            .position('bottom right'));
                    });
            };

        } else {
            $location.path('/');
        }

    }


    AnuncioscreateCtrl.$inject = ['Anuncios', '$location', '$mdToast', 'SecurityFilter'];

    function AnuncioscreateCtrl(Anuncios, $location, $mdToast, SecurityFilter) {
      var vm = this;
      vm.anuncios={
        fechaInicio: new Date(),
        fechaFin: new Date()
      }
      /*
    vm.minDate = new Date(
      vm.anuncios.fechaInicio.getFullYear(),
      vm.anuncios.fechaInicio.getMonth(),

      vm.anuncios.fechaInicio.getDate()
    );
    */
    vm.minDate = new Date();
  //var dias= new Date() - vm.anuncios.fechaInicio.getDate();
  vm.change=function(){
    vm.anuncios.fechaFin = new Date();
   var  dias;
    var fechaActual=new Date();
    var dias =   vm.anuncios.fechaInicio.getDate() - fechaActual.getDate();
    vm.minDateFin = new Date(
      vm.anuncios.fechaFin.getFullYear(),
      vm.anuncios.fechaFin.getMonth(),
    //  vm.anuncios.fechaFin.getDay() + dias,
      vm.anuncios.fechaFin.getDate() + dias
    );
  };




        if (SecurityFilter.isEmp() || SecurityFilter.isAdmin()) {
            vm.create = function() {
                console.log(vm.anuncios);
                Anuncios.save(vm.anuncios, function() {
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Anuncio creado exitosamente...')
                        .position('bottom right'));
                    //$location.path('/anuncios');
                });
            };
        } else {
            $location.path('/');
        }
    }

    AnunciosUpdateCtrl.$inject = ['Anuncios', '$mdToast', '$stateParams', '$location', 'SecurityFilter'];

    function AnunciosUpdateCtrl(Anuncios, $mdToast, $stateParams, $location, SecurityFilter) {
      var vm = this;
      vm.anuncios={
        fechaInicio: new Date(),
        fechaFin: new Date()
      }
      /*
    vm.minDate = new Date(
      vm.anuncios.fechaInicio.getFullYear(),
      vm.anuncios.fechaInicio.getMonth(),

      vm.anuncios.fechaInicio.getDate()
    );
    */
    vm.minDate = new Date();
  //var dias= new Date() - vm.anuncios.fechaInicio.getDate();
  vm.change=function(){
    vm.anuncios.fechaFin = new Date();
   var  dias;
    var fechaActual=new Date();
    var dias =   vm.anuncios.fechaInicio.getDate() - fechaActual.getDate();
    vm.minDateFin = new Date(
      vm.anuncios.fechaFin.getFullYear(),
      vm.anuncios.fechaFin.getMonth(),
    //  vm.anuncios.fechaFin.getDay() + dias,
      vm.anuncios.fechaFin.getDate() + dias
    );
  };

        if (SecurityFilter.isEmp() || SecurityFilter.isAdmin()) {
            vm.id = $stateParams.idAnuncio;
            vm.anuncios = Anuncios.get({
                idAnuncio: this.id
            }).$promise.then(function(data) {
                vm.anuncios = data;
                vm.anuncios.fechaInicio = new Date(vm.anuncios.fechaInicio);
                vm.anuncios.fechaFin = new Date(vm.anuncios.fechaFin);
                console.log(vm.anuncios.estado);
            });

            vm.update = function() {
                Anuncios.update(vm.anuncios, function() {
                    //$location.path('/anuncios');
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Se ha  actualizado el  Anuncio...')
                        .position('bottom right'));
                });
            };
        } else {
            $location.path('/');
        }
    }


})();
