(function() {
    'use strict';

    angular.module('app.anuncios.directivas', [

    ]).directive('anuncios', anuncios)
        .directive('createanuncios', createanuncios)
        .directive('updateanuncios', updateanuncios)
        .directive('misanuncios', misanuncios);

    function anuncios() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/anuncios/listanuncios.html',
            controller: 'AnunciosCtrl',
            controllerAs: 'vm'
        };
    }

    function misanuncios() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/anuncios/misanuncios.html',
            controller: 'MisAnunciosCtrl',
            controllerAs: 'vm'
        };
    }

    function createanuncios() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/anuncios/create.html',
            controller: 'AnuncioscreateCtrl',
            controllerAs: 'vm'
        };
    }

    function updateanuncios() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/anuncios/editar.html',
            controller: 'AnunciosUpdateCtrl',
            controllerAs: 'vm'
        };
    }

})();
