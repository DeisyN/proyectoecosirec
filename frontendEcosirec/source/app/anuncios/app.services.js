(function () {
    'use strict';
    angular.module('app.anuncios.services', [])
       .factory('Anuncios', Anuncios);

    Anuncios.$inject = ['$resource','BASEURL'];

    //Este servicio nos provee de los métodos GET POST PUT DELETE
    function Anuncios($resource, BASEURL) {
      return $resource(BASEURL + '/anuncios/:idAnuncio',
      {
         idAnuncio:'@idAnuncio'
      },{
        'update': {method: 'PUT'},
        'inhabilitar': {
          url: BASEURL+'/anuncios/inhabilitar/:idAnuncio',
          method: 'PUT',
          //isArray: true,
          params: {idAnuncio: '@idAnuncio'}
        },
          searchIdUsuario:{
          url: BASEURL + '/anuncios/usuario',
          method: 'GET',
          isArray: true
        }
    });
  }
})();
