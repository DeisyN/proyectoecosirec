(function () {
    'use strict';

    angular.module('app.header.controller', [

    ]).controller('HeaderCtrl', HeaderCtrl);

    HeaderCtrl.$inject = ['Usuarios', 'SecurityFilter'];

  function HeaderCtrl(Usuarios, SecurityFilter){
    var vm = this;
    vm.usuario = Usuarios.get({
        idUsuario: SecurityFilter.getCurrentId()
    });
  }

})();
