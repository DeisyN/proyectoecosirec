(function () {
    'use strict';

    angular.module('app.header.directivas', [

    ]).directive('encabezado', encabezado)
    .directive('encabezado2',encabezado2);

    function encabezado() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/header/header.html',
            controller:'LoginCtrl',
            controllerAs:'vm'
        };
    }

    function encabezado2() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/header/header2.html',
            controller:'LoginCtrl',
            controllerAs:'vm'
        };
    }




})();
