(function () {
    'use strict';

    angular.module('app.ayuda.directivas', [

    ]).directive('ayuda', ayuda);

  function ayuda(){
    return{
      scope: {},
      restrict: 'EA',
      templateUrl: 'app/ayuda/ayuda.html',
      controller: 'AyudaCtrl',
      controllerAs: 'vm'
    };
  }

})();
