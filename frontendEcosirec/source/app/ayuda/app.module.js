(function () {
    'use strict';

    angular.module('app.ayuda', [
        'app.ayuda.controller',
        'app.ayuda.directivas',
        'app.ayuda.router'

    ]);

})();
