(function () {
    'use strict';

    angular.module('app.ayuda.router', [
        'app.ayuda.controller'
    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('ayuda', {
                url: '/ayuda',
                template: '<ayuda></ayuda>'
            });
    }
})();
