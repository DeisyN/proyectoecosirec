(function (){
  'use strict';
  angular.module('app.pedidos.services', [])
  .factory('Pedidos', Pedidos)
  .factory('DetallePedidos', DetallePedidos)
  .factory('Estados', Estados);

  Pedidos.$inject = ['$resource', 'BASEURL'];

  function Pedidos($resource, BASEURL) {
    return $resource(BASEURL + '/pedidos/:idPedido',
    {
      idPedido: '@idPedido'
    },
    {
      'update': {method: 'PUT'}
    });
  }

  DetallePedidos.$inject = ['$resource', 'BASEURL'];

  function DetallePedidos($resource, BASEURL) {
    return $resource(BASEURL + '/detallePedidos/:id',
    {id: '@id'},
    {'update': {method:'PUT'},
      searchIdUsuario:{
      url: BASEURL + '/detallePedidos/usuario',
      method: 'GET',
      isArray: true
    }
    });

  }

  Estados.$inject = ['$resource', 'BASEURL'];
  function Estados($resource, BASEURL){
    return $resource(BASEURL + '/estados'
    // {id: '@id'}
  );
  }


})();
