(function() {
    'use strict';

    angular.module('app.pedidos.controller', [])
        .controller('DetallePedidosListCtrl', DetallePedidosListCtrl)
        .controller('MisPedidosListCtrl', MisPedidosListCtrl)
        .controller('PedidosCreateCtrl', PedidosCreateCtrl);

    DetallePedidosListCtrl.$inject = ['DetallePedidos', 'Pedidos', '$mdToast', 'Estados', '$location', 'SecurityFilter'];

    function DetallePedidosListCtrl(DetallePedidos, Pedidos, $mdToast, Estados, $location, SecurityFilter) {
        var vm = this;
        vm.query = {
            order: 'name',
            limit: 5,
            page: 1

        };
        
        vm.estados = Estados.query();

        if (SecurityFilter.isAdmin()) {

            vm.detallePedidoslist = DetallePedidos.query().$promise.then(function(data) {
                vm.detallePedidoslist = data;
            });

            vm.inhabilitar = function(pedido) {

                Pedidos.update({
                        idPedido: pedido.idPedido
                    }, pedido).$promise
                    .then(function(data) {
                        console.log("realizado con exito");
                        console.log(data);
                        $mdToast.show(
                            $mdToast.simple()
                            .textContent('Estado actualizado exitosamente...')
                            .position('bottom right'));
                    })
                    .catch(function(data) {
                        console.log("fallo en inhabilitar");
                        console.log(data);
                        $mdToast.show(
                            $mdToast.simple()
                            .textContent('Error al actualizar el estado...')
                            .position('bottom right'));
                    });
            };
        } else {
            $location.path('/');
        }

    }

    MisPedidosListCtrl.$inject = ['DetallePedidos', 'Pedidos', '$mdToast', 'Estados', '$location', 'SecurityFilter'];

    function MisPedidosListCtrl(DetallePedidos, Pedidos, $mdToast, Estados, $location, SecurityFilter) {
        var vm = this;
        vm.query = {
            order: 'name',
            limit: 5,
            page: 1

        };

        vm.estados = Estados.query();

        if (SecurityFilter.isEmp()) {
            vm.detallePedidos = DetallePedidos.searchIdUsuario().$promise.then(function(data) {
                  vm.detallePedidos = data;
            });
        } else {
            $location.path('/');
        }

    }

    PedidosCreateCtrl.$inject = ['Pedidos', 'DetallePedidos', 'Servicios', '$stateParams', '$location', '$mdToast', 'SecurityFilter'];

    function PedidosCreateCtrl(Pedidos, DetallePedidos, Servicios, $stateParams, $location, $mdToast, SecurityFilter) {
        var vm = this;
        if (SecurityFilter.isEmp()) {
            vm.date = new Date();
            vm.date.getDate();
            vm.resultado = null;
            vm.servicios = Servicios.get({
                idServicio: $stateParams.idServicio
            }).$promise.then(function(data) {
                vm.detalle.precio = data.precio;
                vm.paquete = {};
                vm.paquete.nombre = data.nombre;
                vm.paquete.duracionMeses = data.duracionMeses;
            });


            vm.calculo = function () {
                vm.resultado = vm.detalle.cantidad * vm.detalle.precio;
                return vm.resultado;
            };

            vm.detalle = {};
            //vm.datefin =new Date();
            vm.datefin = function() {
                vm.calculo();
                vm.detalle.fechaFin = new Date();
                vm.detalle.fechaFin.setFullYear(vm.detalle.fechaFin.getFullYear(), (vm.detalle.fechaFin.getMonth() + (vm.detalle.cantidad * vm.paquete.duracionMeses)));
                //vm.detalle.fechaFin = vm.fechaFin;
            };

            //vm.datefin.setFullYear(vm.datefin.getFullYear(), vm.datefin.getMonth() + vm.cantidad);

            vm.create = function() {
                vm.pedidos = {};
                vm.pedidos.fecha = vm.date;
                //vm.pedidos.total = vm.detalle.cantidad*vm.detalle.precio;
                vm.detalle.fechaInicio = vm.date;
                //vm.detalle.fechaFin = vm.datefin;
                console.log(vm.pedidos);
                Pedidos.save(vm.pedidos).$promise.then(function(data) {
                    console.log(data.idPedido);
                    vm.detalle.detallePedidosPK = {};
                    vm.detalle.detallePedidosPK.idServicio = $stateParams.idServicio;
                    vm.detalle.detallePedidosPK.idPedido = data.idPedido;
                    vm.detalle.pedidos = {
                        idPedido: data.idPedido
                    };
                    vm.detalle.servicios = {
                        idServicio: $stateParams.idServicio
                    };
                    console.log(vm.detalle);
                    DetallePedidos.save(vm.detalle);
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Pedido realizado exitosamente...')
                        .position('bottom right'));
                    $location.path('/portafolio');
                });
            };
        } else {
            $location.path('/');
        }
    }

})();
