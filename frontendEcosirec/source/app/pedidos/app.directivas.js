(function() {
    'use strict';

    angular.module('app.pedidos.directive', [])
        .directive('detallepedidoslist', detallepedidoslist)
        .directive('mispedidoslist', mispedidoslist)
        .directive('pedidoscreate', pedidoscreate);

    function detallepedidoslist() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/pedidos/listdetalle.html',
            controller: 'DetallePedidosListCtrl',
            controllerAs: 'vm'
        };
    }

    function mispedidoslist() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/pedidos/mispedidos.html',
            controller: 'MisPedidosListCtrl',
            controllerAs: 'vm'
        };
    }

    function pedidoscreate() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/pedidos/crearpedido.html',
            controller: 'PedidosCreateCtrl',
            controllerAs: 'vm'
        };
    }

})();
