(function() {
    'use strict';

    angular.module('app.pedidos.router', [])
        .config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('detallepedidoslist', {
                url: '/Pedidos/list',
                template: '<detallepedidoslist></detallepedidoslist>'
            }).state('mispedidoslist', {
                url: '/MisPedidos/list',
                template: '<mispedidoslist></mispedidoslist>'
            }).state('pedidoscreate', {
                url: '/pedidos/create/:idServicio',
                template: '<pedidoscreate></pedidoscreate>'
            });
    }

})();
