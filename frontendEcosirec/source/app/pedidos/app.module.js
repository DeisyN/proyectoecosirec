(function(){
  'use strict';

  angular.module('app.pedidos', [
    'app.pedidos.controller',
    'app.pedidos.directive',
    'app.pedidos.router',
    'app.pedidos.services'
  ]);
})();
