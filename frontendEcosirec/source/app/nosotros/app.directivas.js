(function () {
    'use strict';

    angular.module('app.nosotros.directivas', [

    ]).directive('nosotros', nosotros);

    function nosotros() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/nosotros/nosotros.html',
            controller: 'nosotrosCtrl',
            controllerAs: 'vm'
        };
    }

})();
