(function () {
    'use strict';

    angular.module('app.nosotros.router', [
        'app.nosotros.controller'
    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('nosotros', {
                url: '/nosotros',
                template: '<nosotros></nosotros>'
            });
    }
})();
