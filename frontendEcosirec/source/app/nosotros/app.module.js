(function () {
    'use strict';

    angular.module('app.nosotros', [
        'app.nosotros.controller',
        'app.nosotros.directivas',
        'app.nosotros.router'

    ]);

})();
