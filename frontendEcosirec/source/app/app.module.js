(function () {
    'use strict';

    angular.module('app', [
        'ui.router',
        'ngResource',
        'ngMaterial',
        'ngMessages',
        'ngAnimate',
        'ngAria',
        'angular.filter',
        'satellizer',
        'ngTable',
        'md.data.table',
        // 'templates',
        'app.config',
        'app.securityFilter',
        'app.inicio',
        'app.header',
        'app.admin',
        'app.nosotros',
        'app.portafolio',
        'app.footer',
        'app.usuarios',
        'app.login',
        'app.auditoria_usuarios',
        'app.reciclar',
        'app.anuncios',
        'app.imagenes',
        'app.ayuda',
        'app.papelera',
        'app.pedidos',
        'app.recuperarpass'

    ])  .config(function ($mdThemingProvider) {

      $mdThemingProvider.definePalette('orangePalette', {
        '50': '#3c948b',
        '100': '#3c948b',
        '200': '#3c948b',
        '300': '#3c948b',
        '400': '#3c948b',
        '500': '#3c948b',
        '600': '#3c948b',
        '700': '#3c948b',
        '800': '#3c948b',
        '900': '#3c948b',
        'A100': '#3c948b',
        'A200': '#3c948b',
        'A400': '#3c948b',
        'A700': '#3c948b',
        'contrastDefaultColor': 'light'
      });

      $mdThemingProvider.theme('default')
        .primaryPalette('orangePalette');

    });

})();
