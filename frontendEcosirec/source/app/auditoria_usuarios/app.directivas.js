(function () {
    'use strict';

    angular.module('app.auditoria_usuarios.directivas',[

    ]).directive('auditoriausuario', auditoriausuario);

    function auditoriausuario() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/auditoria_usuarios/lista.html',
            controller: 'AuditoriaUsuarioCtrl',
            controllerAs: 'vm'
        };
    }
})();
