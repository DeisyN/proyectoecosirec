(function () {
    'use strict';

    angular.module('app.auditoria_usuarios', [
       'app.auditoria_usuarios.controller',
       'app.auditoria_usuarios.directivas',
       'app.auditoria_usuarios.router',
       'app.auditoria_usuarios.services'

    ]);

})();
