(function () {
    'use strict';
    angular.module('app.auditoria_usuarios.services', [])
       .factory('auditoriausuarios', auditoriausuarios);

    auditoriausuarios.$inject = ['$resource','BASEURL'];


    //Este servicio nos provee de los métodos GET POST PUT DELETE
    function auditoriausuarios($resource, BASEURL) {
        return $resource(BASEURL + '/auditoriaUsuarios/:idAuditoriaUsuario',
        //Se debe digitar el id del modelo
        //{ inmuebleId: '@_id' },
        {
          idAuditoriaUsuario:'@idAuditoriaUsuario'
        },{
          update:{
            method:'PUT'
          }
        });
    }

    //De igual manera los factory nos sirven para almacenar información
    //y que nos pueda servir en cualquier controlador o lugar de la aplicación
    //evitando de esta manera hacer variables globales.*/
})();
