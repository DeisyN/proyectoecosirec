(function () {
    'use strict';

    angular.module('app.auditoria_usuarios.router', [
        'app.auditoria_usuarios.controller'
    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('auditoriausuario', {
                url: '/auditoriausuario',
                template: '<auditoriausuario></auditoriausuario>'
            });
      }
})();
