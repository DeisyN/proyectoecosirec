(function () {
 'use strict';

    angular.module('app.auditoria_usuarios.controller', [

    ]).controller('AuditoriaUsuarioCtrl', AuditoriaUsuarioCtrl);

    AuditoriaUsuarioCtrl.$inject = ['auditoriausuarios', '$location', 'SecurityFilter'];
   function AuditoriaUsuarioCtrl(auditoriausuarios, $location, SecurityFilter) {
     var vm = this;

     vm.query = {
         order: 'name',
         limit: 5,
         page: 1

     };
     
     if (SecurityFilter.isAdmin()) {
       vm.auditoriaUsuario = auditoriausuarios.query();
     } else {
       $location.path('/');
      }
    }

})();
