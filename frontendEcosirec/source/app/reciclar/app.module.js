(function () {
    'use strict';

    angular.module('app.reciclar', [
        'app.reciclar.controller',
        'app.reciclar.directivas',
        'app.reciclar.router'

    ]);

})();
