(function () {
    'use strict';

    angular.module('app.reciclar.router', [
        'app.reciclar.controller'
    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('reciclar', {
                url: '/reciclar',
                template: '<reciclar></reciclar>'
            });
    }
})();
