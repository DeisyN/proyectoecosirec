(function () {
    'use strict';

    angular.module('app.reciclar.directivas', [

    ]).directive('reciclar', reciclar);

  function reciclar(){
    return{
      scope: {},
      restrict: 'EA',
      templateUrl: 'app/reciclar/reciclar.html',
      controller: 'ReciclarCtrl',
      controllerAs: 'vm'
    };
  }

})();
